﻿using System;

namespace JLLF.Domain.VOs
{
    public class Telephone
    {
        public string TelephoneNumber { get; private set; }

        public Telephone(string telephoneNumber)
        {
            ValidateEmptyTelephoneNumber(telephoneNumber);
            ValidateNullTelephoneNumber(telephoneNumber);
            ValidateTelephoneNumberLength(telephoneNumber);
            TelephoneNumber = telephoneNumber;
        }

        protected Telephone()
        {

        }

        private static void ValidateEmptyTelephoneNumber(string telephoneNumber)
        {
            if (telephoneNumber == string.Empty)
                throw new ArgumentException("Telephone number cannot be empty!");
        }

        private static void ValidateNullTelephoneNumber(string telephoneNumber)
        {
            if (telephoneNumber == null)
                throw new ArgumentException("Telephone number cannot be null!");
        }

        private static void ValidateTelephoneNumberLength(string telephoneNumber)
        {
            if (telephoneNumber.Length != 13)
                throw new ArgumentException("Telephone number 13 characters, in this format 00 00000-0000!");
        }
    }
}