﻿using JLLF.Domain.DTOs.Responses;
using JLLF.Domain.Entities;
using System.Collections.Generic;
using System.Linq;

namespace JLLF.Service.Mappings.Responses
{
    public class ResponseUserMapping
    {
        public ResponseUserDTO Conversion(User user)
        {
            return new ResponseUserDTO
            {
                Id = user.Id,
                EmailAddress = user.Email.Address,
                Password = user.Password,
                RegistrationDate = user.Registration.RegistrationDate,
                Status = user.Condition.Status
            };
        }

        public IList<ResponseUserDTO> ListConversion(IList<User> users)
        {
            return users.Select(item => Conversion(item)).ToList();
        }
    }
}