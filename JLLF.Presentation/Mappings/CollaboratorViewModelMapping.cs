﻿using JLLF.Domain.DTOs;
using JLLF.Presentation.ViewModels;

namespace JLLF.Presentation.Mappings
{
    public class CollaboratorViewModelMapping
    {
        public CollaboratorDTO Conversion(CollaboratorViewModel collaboratorViewModel)
        {
            return new CollaboratorDTO
            {
                Id = collaboratorViewModel.Id,
                Name = collaboratorViewModel.Name,
                Gender = collaboratorViewModel.Gender,
                CPF = collaboratorViewModel.CPF,
                TelephoneNumber = collaboratorViewModel.TelephoneNumber,
                Function = collaboratorViewModel.Function,
                RegistrationDate = collaboratorViewModel.RegistrationDate,
                Status = collaboratorViewModel.Status
            };
        }

        public CollaboratorViewModel Conversion(CollaboratorDTO collaboratorDTO)
        {
            return new CollaboratorViewModel
            {
                Id = collaboratorDTO.Id,
                Name = collaboratorDTO.Name,
                Gender = collaboratorDTO.Gender,
                CPF = collaboratorDTO.CPF,
                TelephoneNumber = collaboratorDTO.TelephoneNumber,
                Function = collaboratorDTO.Function,
                RegistrationDate = collaboratorDTO.RegistrationDate,
                Status = collaboratorDTO.Status
            };
        }
    }
}