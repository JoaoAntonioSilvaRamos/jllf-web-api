﻿using JLLF.Domain.DTOs;
using JLLF.Domain.Entities;
using System.Collections.Generic;
using System.Linq;

namespace JLLF.Service.Mappings
{
    public class SchedulingMapping
    {
        public Scheduling Conversion(SchedulingDTO schedulingDTO)
        {
            return new Scheduling(schedulingDTO.Id, schedulingDTO.IdCollaboratorDTO, schedulingDTO.IdCustomerDTO, schedulingDTO.IdServiceProvidedDTO, schedulingDTO.DateTime, schedulingDTO.Status);
        }

        public SchedulingDTO Conversion(Scheduling scheduling)
        {
            return new SchedulingDTO
            {
                Id = scheduling.Id,
                IdCollaboratorDTO = scheduling.IdCollaborator,
                IdCustomerDTO = scheduling.IdCustomer,
                IdServiceProvidedDTO = scheduling.IdServiceProvided,
                DateTime = scheduling.DateTime,
                Status = scheduling.Condition.Status
            };
        }

        public ICollection<SchedulingDTO> CollectionConversion(ICollection<Scheduling> schedulings)
        {
            return schedulings.Select(item => Conversion(item)).ToList();
        }
    }
}