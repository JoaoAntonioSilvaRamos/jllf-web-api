﻿using FluentValidation;
using JLLF.Domain.DTOs;
using JLLF.Domain.DTOs.Responses;
using JLLF.Domain.Entities;
using System;
using System.Collections.Generic;

namespace JLLF.Domain.Interfaces.Services
{
    public interface ISchedulingService
    {
        void Create<SchedulingValidation>(SchedulingDTO schedulingDTO) where SchedulingValidation : AbstractValidator<Scheduling>;
        void Update<SchedulingValidation>(SchedulingDTO schedulingDTO) where SchedulingValidation : AbstractValidator<Scheduling>;
        void Delete(int id);
        ResponseSchedulingDTO FindById(int id);
        IList<ResponseSchedulingDTO> FindAll();
        IList<ResponseSchedulingDTO> FindByPeriod(DateTime initialDate, DateTime finalDate);
        ResponseCompositeDTO FindAllCollaboratorsAndCustomersAndServicesProvided();
        ResponseCompositeDTO FindCollaboratorAndCustomerAndSchedulingAndServiceProvidedById(int id);
    }
}