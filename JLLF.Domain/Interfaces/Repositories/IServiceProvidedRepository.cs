﻿using JLLF.Domain.Entities;
using JLLF.Domain.Interfaces.Repositories.Base;
using System.Collections.Generic;

namespace JLLF.Domain.Interfaces.Repositories
{
    public interface IServiceProvidedRepository : IBaseRepository<ServiceProvided>
    {
        ServiceProvided FindById(int id);
        IList<ServiceProvided> FindAll();
    }
}