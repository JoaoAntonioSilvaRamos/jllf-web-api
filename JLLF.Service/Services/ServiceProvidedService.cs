﻿using FluentValidation;
using JLLF.Domain.DTOs;
using JLLF.Domain.DTOs.Responses;
using JLLF.Domain.Entities;
using JLLF.Domain.Interfaces.Repositories;
using JLLF.Domain.Interfaces.Services;
using JLLF.Domain.Interfaces.UoW;
using JLLF.Infra.CrossCutting.Constants;
using JLLF.Infra.CrossCutting.Messages;
using JLLF.Service.Mappings;
using JLLF.Service.Mappings.Responses;
using System;
using System.Collections.Generic;

namespace JLLF.Service.Services
{
    public class ServiceProvidedService : IServiceProvidedService
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IServiceProvidedRepository _serviceProvidedRepository;
        private readonly ServiceProvidedMapping _serviceProvidedMapping = new();
        private readonly ResponseServiceProvidedMapping _responseServiceProvidedMapping = new();

        public ServiceProvidedService(IUnitOfWork unitOfWork, IServiceProvidedRepository serviceProvidedRepository)
        {
            _unitOfWork = unitOfWork;
            _serviceProvidedRepository = serviceProvidedRepository;
        }

        public void Create<ServiceProvidedValidation>(ServiceProvidedDTO serviceProvidedDTO) where ServiceProvidedValidation : AbstractValidator<ServiceProvided>
        {
            var serviceProvided = _serviceProvidedMapping.Conversion(serviceProvidedDTO);

            if (serviceProvided.Id >= GeneralConstants.ONE)
                throw new Exception(GeneralMessages.IdMustBe_0.GetMessage("Service Provided"));

            Validate(serviceProvided, Activator.CreateInstance<ServiceProvidedValidation>());

            _serviceProvidedRepository.Insert(serviceProvided);

            _unitOfWork.Commit();
        }

        public void Update<ServiceProvidedValidation>(ServiceProvidedDTO serviceProvidedDTO) where ServiceProvidedValidation : AbstractValidator<ServiceProvided>
        {
            var serviceProvided = _serviceProvidedMapping.Conversion(serviceProvidedDTO);

            if (serviceProvided.Id <= GeneralConstants.ZERO)
                throw new Exception(GeneralMessages.IdMustBeGreatherThan_0.GetMessage("Service Provided"));

            Validate(serviceProvided, Activator.CreateInstance<ServiceProvidedValidation>());

            _serviceProvidedRepository.Update(serviceProvided);

            _unitOfWork.Commit();
        }

        public void Delete(int id)
        {
            if (id <= GeneralConstants.ZERO)
                throw new Exception(GeneralMessages.IdMustBeGreatherThan_0.GetMessage("Service Provided"));

            _serviceProvidedRepository.Delete(id);

            _unitOfWork.Commit();
        }

        public ResponseServiceProvidedDTO FindById(int id)
        {
            if (id <= GeneralConstants.ZERO)
                throw new Exception(GeneralMessages.IdMustBeGreatherThan_0.GetMessage("Service Provided"));

            var serviceProvidedDB = _serviceProvidedRepository.FindById(id);

            var responseServiceProvidedDTO = _responseServiceProvidedMapping.Conversion(serviceProvidedDB);

            return responseServiceProvidedDTO ?? throw new Exception(GeneralMessages.NotFound.GetMessage("Service Provided"));
        }

        public IList<ResponseServiceProvidedDTO> FindAll()
        {
            var servicesProvidedDB = _serviceProvidedRepository.FindAll();

            var responseServicesProvidedDTO = _responseServiceProvidedMapping.ListConversion(servicesProvidedDB);

            return responseServicesProvidedDTO != null && responseServicesProvidedDTO.Count > 0 ? responseServicesProvidedDTO : throw new Exception(GeneralMessages.NotFound.GetMessage("Services Provided"));
        }

        private static void Validate(ServiceProvided serviceProvided, AbstractValidator<ServiceProvided> validator)
        {
            if (serviceProvided == null)
                throw new Exception(GeneralMessages.NotFound.GetMessage("Service Provided"));

            validator.ValidateAndThrow(serviceProvided);
        }
    }
}