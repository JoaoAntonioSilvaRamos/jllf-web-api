﻿using System;

namespace JLLF.Domain.VOs
{
    public class Email
    {
        public string Address { get; private set; }

        public Email(string address)
        {
            ValidateEmptyEmailAddress(address);
            ValidateNullEmailAddress(address);
            ValidateEmailAddressLength(address);
            Address = address;
        }

        protected Email()
        {

        }

        private static void ValidateEmptyEmailAddress(string address)
        {
            if (address == string.Empty)
                throw new ArgumentException("E-mail address cannot be empty!");
        }

        private static void ValidateNullEmailAddress(string address)
        {
            if (address == null)
                throw new ArgumentException("E-mail address cannot be null!");
        }

        private static void ValidateEmailAddressLength(string address)
        {
            if (address.Length > 255)
                throw new ArgumentException("E-mail address cannot be longer than 255 characters!");
        }
    }
}