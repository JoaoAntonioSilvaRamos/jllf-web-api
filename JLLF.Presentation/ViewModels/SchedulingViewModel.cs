﻿using System;
using System.ComponentModel.DataAnnotations;

namespace JLLF.Presentation.ViewModels
{
    public class SchedulingViewModel
    {
        public int? Id { get; set; }

        [Required(ErrorMessage = "Campo Obrigatório!")]
        [Range(1, 999999999)]
        public int IdCollaboratorViewModel { get; set; }

        [Required(ErrorMessage = "Campo Obrigatório!")]
        [Range(1, 999999999)]
        public int IdCustomerViewModel { get; set; }

        [Required(ErrorMessage = "Campo Obrigatório!")]
        [Range(1, 999999999)]
        public int IdServiceProvidedViewModel { get; set; }

        [Required(ErrorMessage = "Campo Obrigatório!")]
        public DateTime DateTime { get; set; }

        public bool? Status { get; set; }
    }

    public class ResponseSchedulingViewModel : SchedulingViewModel
    {
        public virtual CollaboratorViewModel CollaboratorViewModel { get; set; }
        public virtual CustomerViewModel CustomerViewModel { get; set; }
        public virtual ServiceProvidedViewModel ServiceProvidedViewModel { get; set; }
    }
}