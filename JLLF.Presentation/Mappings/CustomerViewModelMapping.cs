﻿using JLLF.Domain.DTOs;
using JLLF.Presentation.ViewModels;

namespace JLLF.Presentation.Mappings
{
    public class CustomerViewModelMapping
    {
        public CustomerDTO Conversion(CustomerViewModel customerViewModel)
        {
            return new CustomerDTO
            {
                Id = customerViewModel.Id,
                Name = customerViewModel.Name,
                Gender = customerViewModel.Gender,
                CPF = customerViewModel.CPF,
                TelephoneNumber = customerViewModel.TelephoneNumber,
                EmailAddress = customerViewModel.EmailAddress,
                RegistrationDate = customerViewModel.RegistrationDate,
                Status = customerViewModel.Status
            };
        }

        public CustomerViewModel Conversion(CustomerDTO customerDTO)
        {
            return new CustomerViewModel
            {
                Id = customerDTO.Id,
                Name = customerDTO.Name,
                Gender = customerDTO.Gender,
                CPF = customerDTO.CPF,
                TelephoneNumber = customerDTO.TelephoneNumber,
                EmailAddress = customerDTO.EmailAddress,
                RegistrationDate = customerDTO.RegistrationDate,
                Status = customerDTO.Status
            };
        }
    }
}