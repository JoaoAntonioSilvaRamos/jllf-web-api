using JLLF.Domain.Interfaces.Repositories;
using JLLF.Domain.Interfaces.Services;
using JLLF.Domain.Interfaces.UoW;
using JLLF.Infra.Data.Context;
using JLLF.Infra.Data.Repositories;
using JLLF.Infra.Data.UoW;
using JLLF.Service.Services;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Net.Http.Headers;
using Microsoft.OpenApi.Models;
using System;

namespace JLLF.Application
{
    public class Startup
    {
        public IConfiguration _configuration { get; }

        public Startup(IConfiguration configuration)
        {
            _configuration = configuration;
        }

        public void ConfigureServices(IServiceCollection services)
        {
            var connectionString = _configuration["MySqlConnection:MySqlConnectionString"];

            services.AddDbContext<JLLFContext>(options => options.UseMySql(connectionString, new MySqlServerVersion(new Version(8, 0, 25))));

            services.AddScoped<IUnitOfWork, UnitOfWork>();

            services.AddScoped<ICollaboratorService, CollaboratorService>();
            services.AddScoped<ICollaboratorRepository, CollaboratorRepository>();

            services.AddScoped<ICustomerService, CustomerService>();
            services.AddScoped<ICustomerRepository, CustomerRepository>();

            services.AddScoped<ISchedulingService, SchedulingService>();
            services.AddScoped<ISchedulingRepository, SchedulingRepository>();

            services.AddScoped<IServiceProvidedService, ServiceProvidedService>();
            services.AddScoped<IServiceProvidedRepository, ServiceProvidedRepository>();

            services.AddScoped<IUserService, UserService>();
            services.AddScoped<IUserRepository, UserRepository>();

            services.AddControllers();

            services.AddMvc(options =>
            {
                options.RespectBrowserAcceptHeader = true;
                options.FormatterMappings.SetMediaTypeMappingForFormat("xml", MediaTypeHeaderValue.Parse("text/xml"));
                options.FormatterMappings.SetMediaTypeMappingForFormat("json", MediaTypeHeaderValue.Parse("application/json"));

            }).AddXmlSerializerFormatters();

            services.AddSwaggerGen(s =>
            {
                s.SwaggerDoc("v1", new OpenApiInfo
                { 
                    Title = "API de Controle e Gest�o de Agendamentos de Servi�os",
                    Version = "v1",
                    Description = "API de Controle e Gest�o de Agendamentos de Servi�os"
                });
            });

            services.AddApiVersioning();
        }

        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                app.UseSwagger();
                app.UseSwaggerUI(c => c.SwaggerEndpoint("/swagger/v1/swagger.json", "API JLLF v1"));
            }

            app.UseHttpsRedirection();

            app.UseRouting();

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
    }
}