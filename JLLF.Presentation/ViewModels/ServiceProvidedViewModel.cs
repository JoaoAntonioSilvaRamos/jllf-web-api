﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace JLLF.Presentation.ViewModels
{
    public class ServiceProvidedViewModel
    {
        public int? Id { get; set; }

        [Required(ErrorMessage = "Campo Obrigatório!")]
        [MinLength(1, ErrorMessage = "Campo Deve Conter No Mínimo 1 Caracter!")]
        [MaxLength(255, ErrorMessage = "Campo Deve Conter No Máximo 255 Caracteres!")]
        public string Description { get; set; }

        [Required(ErrorMessage = "Campo Obrigatório!")]
        public decimal Price { get; set; }

        public DateTime? RegistrationDate { get; set; }

        public bool? Status { get; set; }
    }

    public class ResponseServiceProvidedViewModel : ServiceProvidedViewModel
    {
        public virtual ICollection<SchedulingViewModel> SchedulingsViewModelListByServiceProvidedViewModel { get; set; }
    }
}