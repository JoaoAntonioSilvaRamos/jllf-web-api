﻿using FluentValidation;
using JLLF.Domain.DTOs;
using JLLF.Domain.DTOs.Responses;
using JLLF.Domain.Entities;
using JLLF.Domain.Interfaces.Repositories;
using JLLF.Domain.Interfaces.Services;
using JLLF.Domain.Interfaces.UoW;
using JLLF.Infra.CrossCutting.Constants;
using JLLF.Infra.CrossCutting.EmailSendingSetup;
using JLLF.Infra.CrossCutting.Messages;
using JLLF.Service.Mappings;
using JLLF.Service.Mappings.Responses;
using System;
using System.Collections.Generic;
using System.Text;

namespace JLLF.Service.Services
{
    public class SchedulingService : ISchedulingService
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly ISchedulingRepository _schedulingRepository;
        private readonly ICollaboratorRepository _collaboratorRepository;
        private readonly ICustomerRepository _customerRepository;
        private readonly IServiceProvidedRepository _serviceProvidedRepository;
        private readonly SchedulingMapping _schedulingMapping = new();
        private readonly ResponseSchedulingMapping _responseSchedulingMapping = new();
        private readonly ResponseCollaboratorMapping _responseCollaboratorMapping = new();
        private readonly ResponseCustomerMapping _responseCustomerMapping = new();
        private readonly ResponseServiceProvidedMapping _responseServiceProvidedMapping = new();

        public SchedulingService(IUnitOfWork unitOfWork, ISchedulingRepository schedulingRepository,
            ICollaboratorRepository collaboratorRepository, ICustomerRepository customerRepository, IServiceProvidedRepository serviceProvidedRepository)
        {
            _unitOfWork = unitOfWork;
            _schedulingRepository = schedulingRepository;
            _collaboratorRepository = collaboratorRepository;
            _customerRepository = customerRepository;
            _serviceProvidedRepository = serviceProvidedRepository;
        }

        public void Create<SchedulingValidation>(SchedulingDTO schedulingDTO) where SchedulingValidation : AbstractValidator<Scheduling>
        {
            var scheduling = _schedulingMapping.Conversion(schedulingDTO);

            if (scheduling.Id >= GeneralConstants.ONE)
                throw new Exception(GeneralMessages.IdMustBe_0.GetMessage("Scheduling"));

            Validate(scheduling, Activator.CreateInstance<SchedulingValidation>());

            _schedulingRepository.Insert(scheduling);

            _unitOfWork.Commit();

            var generalEmailDTO = FillInDataForSendingSchedulingEmail(schedulingDTO);

            GeneralEmails.SendEmail(generalEmailDTO);
        }

        public void Update<SchedulingValidation>(SchedulingDTO schedulingDTO) where SchedulingValidation : AbstractValidator<Scheduling>
        {
            var scheduling = _schedulingMapping.Conversion(schedulingDTO);

            if (scheduling.Id <= GeneralConstants.ZERO)
                throw new Exception(GeneralMessages.IdMustBeGreatherThan_0.GetMessage("Scheduling"));

            Validate(scheduling, Activator.CreateInstance<SchedulingValidation>());

            _schedulingRepository.Update(scheduling);

            _unitOfWork.Commit();

            if (schedulingDTO.Status == false)
            {
                var generalEmailDTO = FillInTheDetailsToSendReschedulingEmail(schedulingDTO);

                GeneralEmails.SendEmail(generalEmailDTO);
            }
        }

        public void Delete(int id)
        {
            if (id <= GeneralConstants.ZERO)
                throw new Exception(GeneralMessages.IdMustBeGreatherThan_0.GetMessage("Scheduling"));

            _schedulingRepository.Delete(id);

            _unitOfWork.Commit();
        }

        public ResponseSchedulingDTO FindById(int id)
        {
            if (id <= GeneralConstants.ZERO)
                throw new Exception(GeneralMessages.IdMustBeGreatherThan_0.GetMessage("Scheduling"));

            var schedulingDB = _schedulingRepository.FindById(id);

            var responseSchedulingDTO = _responseSchedulingMapping.Conversion(schedulingDB);

            return responseSchedulingDTO ?? throw new Exception(GeneralMessages.NotFound.GetMessage("Scheduling"));
        }

        public IList<ResponseSchedulingDTO> FindAll()
        {
            var schedulingsDB = _schedulingRepository.FindAll();

            var responseSchedulingsDTO = _responseSchedulingMapping.ListConversion(schedulingsDB);

            return responseSchedulingsDTO != null && responseSchedulingsDTO.Count > 0 ? responseSchedulingsDTO : throw new Exception(GeneralMessages.NotFound.GetMessage("Schedulings"));
        }

        public IList<ResponseSchedulingDTO> FindByPeriod(DateTime initialDate, DateTime finalDate)
        {
            var schedulingsDB = _schedulingRepository.FindByPeriod(initialDate.Date, finalDate.Date);

            var responseSchedulingsDTO = _responseSchedulingMapping.ListConversion(schedulingsDB);

            return responseSchedulingsDTO != null && responseSchedulingsDTO.Count > 0 ? responseSchedulingsDTO : throw new Exception(GeneralMessages.NotFound.GetMessage("Schedulings"));
        }

        public ResponseCompositeDTO FindAllCollaboratorsAndCustomersAndServicesProvided()
        {
            ResponseCompositeDTO responseCompositeDTO = new()
            {
                ResponseCollaboratorsDTO = _responseCollaboratorMapping.ListConversion(_collaboratorRepository.FindAll()),
                ResponseCustomersDTO = _responseCustomerMapping.ListConversion(_customerRepository.FindAll()),
                ResponseServicesProvidedDTO =  _responseServiceProvidedMapping.ListConversion(_serviceProvidedRepository.FindAll())
            };

            return responseCompositeDTO;
        }

        public ResponseCompositeDTO FindCollaboratorAndCustomerAndSchedulingAndServiceProvidedById(int id)
        {
            var schedulingDB = _schedulingRepository.FindById(id);

            ResponseCompositeDTO responseCompositeDTO = new ResponseCompositeDTO
            {
                Id = schedulingDB.Id,
                IdCollaboratorDTO = schedulingDB.IdCollaborator,
                IdCustomerDTO = schedulingDB.IdCustomer,
                IdServiceProvidedDTO = schedulingDB.IdServiceProvided,
                DateTime = schedulingDB.DateTime,
                ResponseCollaboratorsDTO = _responseCollaboratorMapping.ListConversion(_collaboratorRepository.FindAll()),
                ResponseCustomersDTO = _responseCustomerMapping.ListConversion(_customerRepository.FindAll()),
                ResponseServicesProvidedDTO = _responseServiceProvidedMapping.ListConversion(_serviceProvidedRepository.FindAll())
            };

            return responseCompositeDTO;
        }

        private static void Validate(Scheduling scheduling, AbstractValidator<Scheduling> validator)
        {
            if (scheduling == null)
                throw new Exception(GeneralMessages.NotFound.GetMessage("Scheduling"));

            validator.ValidateAndThrow(scheduling);
        }

        private GeneralEmailDTO FillInDataForSendingSchedulingEmail(SchedulingDTO schedulingDTO)
        {
            var customer = _customerRepository.FindById(schedulingDTO.IdCustomerDTO);

            var serviceProvided = _serviceProvidedRepository.FindById(schedulingDTO.IdServiceProvidedDTO);

            var bodyEmail = FillInTheBodyEmail(schedulingDTO.DateTime);

            GeneralEmailDTO generalEmailDTO = new()
            {
                Receiver = customer.Email.Address,
                Subject = "Agendamento " + serviceProvided.Description,
                Body = bodyEmail
            };

            return generalEmailDTO;
        }

        private GeneralEmailDTO FillInTheDetailsToSendReschedulingEmail(SchedulingDTO schedulingDTO)
        {
            var customer = _customerRepository.FindById(schedulingDTO.IdCustomerDTO);

            var serviceProvided = _serviceProvidedRepository.FindById(schedulingDTO.IdServiceProvidedDTO);

            var bodyEmail = FillInTheBodyEmail(schedulingDTO.DateTime);

            GeneralEmailDTO generalEmailDTO = new()
            {
                Receiver = customer.Email.Address,
                Subject = "Reagendamento " + serviceProvided.Description,
                Body = bodyEmail
            };

            return generalEmailDTO;
        }

        private static string FillInTheBodyEmail(DateTime schedulingDateTime)
        {
            StringBuilder stringBuilder = new();

            stringBuilder.Append("<!DOCTYPE html>");
            stringBuilder.Append("<html>");
            stringBuilder.Append("<body>");
            stringBuilder.Append("<div>");
            stringBuilder.Append("<label style='font-weight:bold'>Data: " + schedulingDateTime.Date.ToShortDateString() + "</label>");
            stringBuilder.Append("<br />");
            stringBuilder.Append("<label style='font-weight:bold'>Horário: " + schedulingDateTime.TimeOfDay + "</label>");
            stringBuilder.Append("<br />");
            stringBuilder.Append("<br />");
            stringBuilder.Append("<label style='font-weight:bold'>Atenciosamente,</label>");
            stringBuilder.Append("<br />");
            stringBuilder.Append("<br />");
            stringBuilder.Append("<label style='font-weight:bold'>João Antônio Silva Ramos</label>");
            stringBuilder.Append("</div>");
            stringBuilder.Append("</body>");
            stringBuilder.Append("</html>");

            return stringBuilder.ToString();
        }
    }
}