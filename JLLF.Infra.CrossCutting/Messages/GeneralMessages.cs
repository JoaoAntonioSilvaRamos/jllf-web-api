﻿namespace JLLF.Infra.CrossCutting.Messages
{
    public class GeneralMessages
    {
        public static GeneralMessages IdMustBe_0 { get { return new GeneralMessages("{0} Id must be 0!"); } }

        public static GeneralMessages IdMustBeGreatherThan_0 { get { return new GeneralMessages("{0} Id must be greater than 0!"); } }

        public static GeneralMessages NotFound { get { return new GeneralMessages("{0} Not Found!"); } }

        private string Message { get; set; }

        private GeneralMessages(string message)
        { 
            Message = message; 
        }

        public string GetMessage(params object[] args)
        {
            try
            {
                return string.Format(Message, args);
            } 
            catch
            { 
                return Message;
            }
        }
    }
}