﻿using JLLF.Domain.DTOs;
using JLLF.Domain.Entities;

namespace JLLF.Service.Mappings
{
    public class UserMapping
    {
        public User Conversion(UserDTO userDTO)
        {
            return new User(userDTO.Id, userDTO.EmailAddress, userDTO.Password, userDTO.RegistrationDate, userDTO.Status);
        }

        public UserDTO Conversion(User user)
        {
            return new UserDTO
            {
                Id = user.Id,
                EmailAddress = user.Email.Address,
                Password = user.Password,
                RegistrationDate = user.Registration.RegistrationDate,
                Status = user.Condition.Status
            };
        }
    }
}