﻿using JLLF.Domain.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace JLLF.Infra.Data.EntitiesConfigurations
{
    public class SchedulingEntityConfiguration : IEntityTypeConfiguration<Scheduling>
    {
        public void Configure(EntityTypeBuilder<Scheduling> entity)
        {
            entity.ToTable("Schedulings");

            entity.HasKey(s => s.Id);

            entity.Property(s => s.DateTime)
                .IsRequired()
                .HasColumnName("DateTime")
                .HasColumnType("DateTime");

            entity.OwnsOne(c => c.Condition, condition =>
            {
                condition.Property(c => c.Status)
                    .IsRequired()
                    .HasColumnName("Status")
                    .HasColumnType("Boolean");
            });

            entity.HasOne(s => s.Collaborator)
                .WithMany(s => s.SchedulingsListByCollaborator)
                .HasForeignKey(s => s.IdCollaborator);

            entity.HasOne(s => s.Customer)
                .WithMany(s => s.SchedulingsListByCustomer)
                .HasForeignKey(s => s.IdCustomer);

            entity.HasOne(s => s.ServiceProvided)
                .WithMany(s => s.SchedulingsListByServiceProvided)
                .HasForeignKey(s => s.IdServiceProvided);
        }
    }
}