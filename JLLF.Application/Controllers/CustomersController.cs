﻿using JLLF.Application.Controllers.Base;
using JLLF.Domain.DTOs;
using JLLF.Domain.DTOs.Responses.Base;
using JLLF.Domain.Interfaces.Services;
using JLLF.Domain.Validations;
using Microsoft.AspNetCore.Mvc;
using Swashbuckle.AspNetCore.Annotations;
using System;
using System.ComponentModel.DataAnnotations;

namespace JLLF.Application.Controllers
{
    [ApiVersion("1")]
    [Route("api/[controller]/v{version:apiVersion}")]
    public class CustomersController : BaseController
    {
        private readonly ICustomerService _customerService;

        public CustomersController(ICustomerService customerService)
        {
            _customerService = customerService;
        }

        [HttpPost]
        [SwaggerResponse((201), Type = typeof(ResponseBaseDTO))]
        [SwaggerResponse((400), Type = typeof(ResponseBaseDTO))]
        [SwaggerResponse((404), Type = typeof(ResponseBaseDTO))]
        public IActionResult Post([FromBody] CustomerDTO customerDTO)
        {
            try
            {
                _customerService.Create<CustomerValidation>(customerDTO);

                return ResponseCreated(customerDTO);
            }
            catch (ArgumentException argumentException)
            {
                return ResponseBadRequest(argumentException);
            }
            catch (Exception exception)
            {
                return ResponseNotFound(exception);
            }
        }

        [HttpPut("{id}")]
        [SwaggerResponse((200), Type = typeof(ResponseBaseDTO))]
        [SwaggerResponse((400), Type = typeof(ResponseBaseDTO))]
        [SwaggerResponse((404), Type = typeof(ResponseBaseDTO))]
        public IActionResult Put([FromBody] CustomerDTO customerDTO)
        {
            try
            {
                _customerService.Update<CustomerValidation>(customerDTO);

                return ResponseOk(customerDTO);
            }
            catch (ArgumentException argumentException)
            {
                return ResponseBadRequest(argumentException);
            }
            catch (Exception exception)
            {
                return ResponseNotFound(exception);
            }
        }

        [HttpDelete]
        [SwaggerResponse((204), Type = typeof(ResponseBaseDTO))]
        [SwaggerResponse((400), Type = typeof(ResponseBaseDTO))]
        [SwaggerResponse((404), Type = typeof(ResponseBaseDTO))]
        public IActionResult Delete([Required] int id)
        {
            try
            {
                _customerService.Delete(id);

                return ResponseNoContent();
            }
            catch (ArgumentException argumentException)
            {
                return ResponseBadRequest(argumentException);
            }
            catch (Exception exception)
            {
                return ResponseNotFound(exception);
            }
        }

        [HttpGet("{id}")]
        [SwaggerResponse((200), Type = typeof(ResponseBaseDTO))]
        [SwaggerResponse((400), Type = typeof(ResponseBaseDTO))]
        [SwaggerResponse((404), Type = typeof(ResponseBaseDTO))]
        public IActionResult GetById([Required] int id)
        {
            try
            {
                return ResponseOk(_customerService.FindById(id));
            }
            catch (ArgumentException argumentException)
            {
                return ResponseBadRequest(argumentException);
            }
            catch (Exception exception)
            {
                return ResponseNotFound(exception);
            }
        }

        [HttpGet]
        [SwaggerResponse((200), Type = typeof(ResponseBaseDTO))]
        [SwaggerResponse((400), Type = typeof(ResponseBaseDTO))]
        [SwaggerResponse((404), Type = typeof(ResponseBaseDTO))]
        public IActionResult GetAll()
        {
            try
            {
                return ResponseOk(_customerService.FindAll());
            }
            catch (ArgumentException argumentException)
            {
                return ResponseBadRequest(argumentException);
            }
            catch (Exception exception)
            {
                return ResponseNotFound(exception);
            }
        }
    }
}