﻿using JLLF.Application.Controllers.Base;
using JLLF.Domain.DTOs;
using JLLF.Domain.DTOs.Responses.Base;
using JLLF.Domain.Interfaces.Services;
using JLLF.Domain.Validations;
using Microsoft.AspNetCore.Mvc;
using Swashbuckle.AspNetCore.Annotations;
using System;
using System.ComponentModel.DataAnnotations;

namespace JLLF.Application.Controllers
{
    [ApiVersion("1")]
    [Route("api/[controller]/v{version:apiVersion}")]
    public class ServicesProvidedController : BaseController
    {
        private readonly IServiceProvidedService _serviceProvidedService;
        
        public ServicesProvidedController(IServiceProvidedService serviceProvidedService)
        {
            _serviceProvidedService = serviceProvidedService;
        }

        [HttpPost]
        [SwaggerResponse((201), Type = typeof(ResponseBaseDTO))]
        [SwaggerResponse((400), Type = typeof(ResponseBaseDTO))]
        [SwaggerResponse((404), Type = typeof(ResponseBaseDTO))]
        public IActionResult Post([FromBody] ServiceProvidedDTO serviceProvidedDTO)
        {
            try
            {
                _serviceProvidedService.Create<ServiceProvidedValidation>(serviceProvidedDTO);

                return ResponseCreated(serviceProvidedDTO);
            }
            catch (ArgumentException argumentException)
            {
                return ResponseBadRequest(argumentException);
            }
            catch (Exception exception)
            {
                return ResponseNotFound(exception);
            }
        }

        [HttpPut("{id}")]
        [SwaggerResponse((200), Type = typeof(ResponseBaseDTO))]
        [SwaggerResponse((400), Type = typeof(ResponseBaseDTO))]
        [SwaggerResponse((404), Type = typeof(ResponseBaseDTO))]
        public IActionResult Put([FromBody] ServiceProvidedDTO serviceProvidedDTO)
        {
            try
            {
                _serviceProvidedService.Update<ServiceProvidedValidation>(serviceProvidedDTO);

                return ResponseOk(serviceProvidedDTO);
            }
            catch (ArgumentException argumentException)
            {
                return ResponseBadRequest(argumentException);
            }
            catch (Exception exception)
            {
                return ResponseNotFound(exception);
            }
        }

        [HttpDelete]
        [SwaggerResponse((204), Type = typeof(ResponseBaseDTO))]
        [SwaggerResponse((400), Type = typeof(ResponseBaseDTO))]
        [SwaggerResponse((404), Type = typeof(ResponseBaseDTO))]
        public IActionResult Delete([Required] int id)
        {
            try
            {
                _serviceProvidedService.Delete(id);

                return ResponseNoContent();
            }
            catch (ArgumentException argumentException)
            {
                return ResponseBadRequest(argumentException);
            }
            catch (Exception exception)
            {
                return ResponseNotFound(exception);
            }
        }

        [HttpGet("{id}")]
        [SwaggerResponse((200), Type = typeof(ResponseBaseDTO))]
        [SwaggerResponse((400), Type = typeof(ResponseBaseDTO))]
        [SwaggerResponse((404), Type = typeof(ResponseBaseDTO))]
        public IActionResult GetById([Required] int id)
        {
            try
            {
                return ResponseOk(_serviceProvidedService.FindById(id));
            }
            catch (ArgumentException argumentException)
            {
                return ResponseBadRequest(argumentException);
            }
            catch (Exception exception)
            {
                return ResponseNotFound(exception);
            }
        }

        [HttpGet]
        [SwaggerResponse((200), Type = typeof(ResponseBaseDTO))]
        [SwaggerResponse((400), Type = typeof(ResponseBaseDTO))]
        [SwaggerResponse((404), Type = typeof(ResponseBaseDTO))]
        public IActionResult GetAll()
        {
            try
            {
                return ResponseOk(_serviceProvidedService.FindAll());
            }
            catch (ArgumentException argumentException)
            {
                return ResponseBadRequest(argumentException);
            }
            catch (Exception exception)
            {
                return ResponseNotFound(exception);
            }
        }
    }
}