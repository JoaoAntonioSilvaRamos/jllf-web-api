﻿using FluentValidation;
using JLLF.Domain.DTOs;
using JLLF.Domain.DTOs.Responses;
using JLLF.Domain.Entities;
using System.Collections.Generic;

namespace JLLF.Domain.Interfaces.Services
{
    public interface IServiceProvidedService
    {
        void Create<ServiceProvidedValidation>(ServiceProvidedDTO serviceProvidedDTO) where ServiceProvidedValidation : AbstractValidator<ServiceProvided>;
        void Update<ServiceProvidedValidation>(ServiceProvidedDTO serviceProvidedDTO) where ServiceProvidedValidation : AbstractValidator<ServiceProvided>;
        void Delete(int id);
        ResponseServiceProvidedDTO FindById(int id);
        IList<ResponseServiceProvidedDTO> FindAll();
    }
}