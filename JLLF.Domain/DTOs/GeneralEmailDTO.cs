﻿namespace JLLF.Domain.DTOs
{
    public class GeneralEmailDTO
    {
        public string Receiver { get; set; }
        public string Subject { get; set; }
        public string Body { get; set; }
    }
}