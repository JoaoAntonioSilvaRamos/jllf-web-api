﻿using JLLF.Domain.DTOs;
using JLLF.Domain.Entities;

namespace JLLF.Service.Mappings
{
    public class CustomerMapping
    {
        public Customer Conversion(CustomerDTO customerDTO)
        {
            return new Customer(customerDTO.Id, customerDTO.Name, customerDTO.Gender, customerDTO.CPF, customerDTO.TelephoneNumber, customerDTO.EmailAddress, customerDTO.RegistrationDate, customerDTO.Status);
        }

        public CustomerDTO Conversion(Customer customer)
        {
            return new CustomerDTO
            {
                Id = customer.Id,
                Name = customer.Person.Name,
                Gender = customer.Person.Gender,
                CPF = customer.Person.CPF,
                TelephoneNumber = customer.Telephone.TelephoneNumber,
                EmailAddress = customer.Email.Address,
                RegistrationDate = customer.Registration.RegistrationDate,
                Status = customer.Condition.Status
            };
        }
    }
}