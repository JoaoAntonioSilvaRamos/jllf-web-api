﻿using FluentValidation;
using JLLF.Domain.Entities;
using System;

namespace JLLF.Domain.Validations
{
    public class ServiceProvidedValidation : AbstractValidator<ServiceProvided>
    {
        public ServiceProvidedValidation()
        {
            RuleFor(sp => sp)
                .NotNull()
                .OnAnyFailure(sp =>
                {
                    throw new ArgumentNullException("Invalid Service Provided!");
                });

            RuleFor(sp => sp.Description)
                .NotEmpty().WithMessage("The description of the service provided cannot be empty!")
                .NotNull().WithMessage("The description of the service provided cannot be null!")
                .Length(1,255).WithMessage("The description of the service provided must be between 1 and 255 characters!");

            RuleFor(sp => sp.Price)
                .NotEmpty().WithMessage("The price of the service provided cannot be empty!")
                .NotNull().WithMessage("The price of the service provided cannot be null!")
                .GreaterThan(0).WithMessage("The price of the service provided must be greater than 0!");
        }
    }
}