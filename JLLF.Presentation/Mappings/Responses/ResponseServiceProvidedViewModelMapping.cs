﻿using JLLF.Domain.DTOs.Responses;
using JLLF.Presentation.ViewModels;
using System.Collections.Generic;
using System.Linq;

namespace JLLF.Presentation.Mappings.Responses
{
    public class ResponseServiceProvidedViewModelMapping
    {
        private SchedulingViewModelMapping schedulingViewModelMapping = new();

        public ResponseServiceProvidedViewModel Conversion(ResponseServiceProvidedDTO responseServiceProvidedDTO)
        {
            return new ResponseServiceProvidedViewModel
            {
                Id = responseServiceProvidedDTO.Id,
                Description = responseServiceProvidedDTO.Description,
                Price = responseServiceProvidedDTO.Price,
                RegistrationDate = responseServiceProvidedDTO.RegistrationDate,
                Status = responseServiceProvidedDTO.Status,
                SchedulingsViewModelListByServiceProvidedViewModel = schedulingViewModelMapping.CollectionConversion(responseServiceProvidedDTO.SchedulingsDTOListByServiceProvidedDTO)
            };
        }

        public IList<ResponseServiceProvidedViewModel> ListConversion(IList<ResponseServiceProvidedDTO> responseServiceProvidedsDTO)
        {
            return responseServiceProvidedsDTO.Select(item => Conversion(item)).ToList();
        }
    }
}