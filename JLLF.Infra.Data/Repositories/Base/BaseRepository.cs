﻿using JLLF.Domain.Interfaces.Repositories.Base;
using JLLF.Infra.Data.Context;
using System.Collections.Generic;
using System.Linq;

namespace JLLF.Infra.Data.Repositories.Base
{
    public class BaseRepository<Entity> : IBaseRepository<Entity> where Entity : class
    {
        private readonly JLLFContext _context;

        public BaseRepository(JLLFContext context)
        {
            _context = context;
        }

        public void Insert(Entity entity)
        {
            _context.Set<Entity>().Add(entity);
        }

        public void Update(Entity entity)
        {
            _context.Set<Entity>().Update(entity);
        }

        public void Delete(int id)
        {
            _context.Set<Entity>().Remove(SelectById(id));
        }

        public Entity SelectById(int id)
        {
            return _context.Set<Entity>().Find(id);
        }

        public IList<Entity> SelectAll()
        {
            return _context.Set<Entity>().ToList();
        }
    }
}