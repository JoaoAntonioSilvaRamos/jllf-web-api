﻿using JLLF.Domain.DTOs.Responses;
using JLLF.Presentation.ViewModels;
using System.Collections.Generic;
using System.Linq;

namespace JLLF.Presentation.Mappings.Responses
{
    public class ResponseSchedulingViewModelMapping
    {
        private CollaboratorViewModelMapping collaboratorViewModelMapping = new();
        private CustomerViewModelMapping customerViewModelMapping = new();
        private ServiceProvidedViewModelMapping serviceProvidedViewModelMapping = new();

        public ResponseSchedulingViewModel Conversion(ResponseSchedulingDTO responseSchedulingDTO)
        {
            return new ResponseSchedulingViewModel
            {
                Id = responseSchedulingDTO.Id,
                IdCollaboratorViewModel = responseSchedulingDTO.IdCollaboratorDTO,
                CollaboratorViewModel = collaboratorViewModelMapping.Conversion(responseSchedulingDTO.CollaboratorDTO),
                IdCustomerViewModel = responseSchedulingDTO.IdCustomerDTO,
                CustomerViewModel = customerViewModelMapping.Conversion(responseSchedulingDTO.CustomerDTO),
                IdServiceProvidedViewModel = responseSchedulingDTO.IdServiceProvidedDTO,
                ServiceProvidedViewModel = serviceProvidedViewModelMapping.Conversion(responseSchedulingDTO.ServiceProvidedDTO),
                DateTime = responseSchedulingDTO.DateTime,
                Status = responseSchedulingDTO.Status
            };
        }

        public IList<ResponseSchedulingViewModel> ListConversion(IList<ResponseSchedulingDTO> responseSchedulingsDTO)
        {
            return responseSchedulingsDTO.Select(item => Conversion(item)).ToList();
        }
    }
}