﻿using JLLF.Domain.Interfaces.Services;
using JLLF.Domain.Validations;
using JLLF.Presentation.Mappings;
using JLLF.Presentation.Mappings.Responses;
using JLLF.Presentation.ViewModels;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;

namespace JLLF.Presentation.Controllers
{
    public class CustomersController : Controller
    {
        private readonly ICustomerService _customerService;
        private readonly CustomerViewModelMapping customerViewModelMapping = new();
        private readonly ResponseCustomerViewModelMapping responseCustomerViewModelMapping = new();

        public CustomersController(ICustomerService customerService)
        {
            _customerService = customerService;
        }

        public IActionResult Index()
        {
            try
            {
                ViewBag.message = TempData["message"] as string;

                IList<ResponseCustomerViewModel> responseCustomerViewModels = responseCustomerViewModelMapping.ListConversion(_customerService.FindAll());

                if (responseCustomerViewModels.Count > 0)
                    return View(responseCustomerViewModels);

                return RedirectToAction("Create");
            }
            catch (Exception)
            {
                return RedirectToAction("Create");
            }
        }

        public IActionResult Create()
        {
            return View();
        }

        [HttpPost]
        public IActionResult Create(CustomerViewModel customerViewModel)
        {
            if (ModelState.IsValid)
            {
                customerViewModel.RegistrationDate = DateTime.Now;
                customerViewModel.Status = true;

                _customerService.Create<CustomerValidation>(customerViewModelMapping.Conversion(customerViewModel));

                TempData["message"] = "Cliente cadastrado com sucesso!".ToString();

                return RedirectToAction("Index");
            }

            return View();
        }

        public IActionResult Update(int id)
        {
            try
            {
                ResponseCustomerViewModel responseCustomerViewModel = responseCustomerViewModelMapping.Conversion(_customerService.FindById(id));

                if (responseCustomerViewModel.Id > 0)
                    return View(responseCustomerViewModel);

                return RedirectToAction("Index");
            }
            catch (Exception)
            {
                return RedirectToAction("Index");
            }
        }

        [HttpPost]
        public IActionResult Update(CustomerViewModel customerViewModel)
        {
            if (ModelState.IsValid)
            {
                customerViewModel.RegistrationDate = DateTime.Now;
                customerViewModel.Status = true;

                _customerService.Update<CustomerValidation>(customerViewModelMapping.Conversion(customerViewModel));

                TempData["message"] = "Cliente atualizado com sucesso!".ToString();

                return RedirectToAction("Index");
            }

            return View();
        }

        public IActionResult UpdateStatus(int id)
        {
            try
            {
                ResponseCustomerViewModel responseCustomerViewModel = responseCustomerViewModelMapping.Conversion(_customerService.FindById(id));

                if (responseCustomerViewModel.Id > 0)
                    return View(responseCustomerViewModel);

                return RedirectToAction("Index");
            }
            catch (Exception)
            {
                return RedirectToAction("Index");
            }
        }

        [HttpPost]
        public IActionResult UpdateStatus(CustomerViewModel customerViewModel)
        {
            if (ModelState.IsValid)
            {
                customerViewModel.RegistrationDate = DateTime.Now;
                customerViewModel.Status = false;

                _customerService.Update<CustomerValidation>(customerViewModelMapping.Conversion(customerViewModel));

                return RedirectToAction("Index");
            }

            return View();
        }

        public IActionResult Details(int id)
        {
            try
            {
                ResponseCustomerViewModel responseCustomerViewModel = responseCustomerViewModelMapping.Conversion(_customerService.FindById(id));

                if (responseCustomerViewModel.Id > 0)
                    return View(responseCustomerViewModel);

                return RedirectToAction("Index");
            }
            catch (Exception)
            {
                return RedirectToAction("Index");
            }
        }
    }
}