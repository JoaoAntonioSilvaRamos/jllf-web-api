﻿using JLLF.Application.Controllers.Base;
using JLLF.Domain.DTOs;
using JLLF.Domain.DTOs.Responses.Base;
using JLLF.Domain.Interfaces.Services;
using JLLF.Domain.Validations;
using Microsoft.AspNetCore.Mvc;
using Swashbuckle.AspNetCore.Annotations;
using System;
using System.ComponentModel.DataAnnotations;

namespace JLLF.Application.Controllers
{
    [ApiVersion("1")]
    [Route("api/[controller]/v{version:apiVersion}")]
    public class UsersController : BaseController
    {
        private readonly IUserService _userService;

        public UsersController(IUserService userService)
        {
            _userService = userService;
        }

        [HttpPost]
        [SwaggerResponse((201), Type = typeof(ResponseBaseDTO))]
        [SwaggerResponse((400), Type = typeof(ResponseBaseDTO))]
        [SwaggerResponse((404), Type = typeof(ResponseBaseDTO))]
        public IActionResult Post([FromBody] UserDTO userDTO)
        {
            try
            {
                _userService.Create<UserValidation>(userDTO);

                return ResponseCreated(userDTO);
            }
            catch (ArgumentException argumentException)
            {
                return ResponseBadRequest(argumentException);
            }
            catch (Exception exception)
            {
                return ResponseNotFound(exception);
            }
        }

        [HttpPut("{id}")]
        [SwaggerResponse((200), Type = typeof(ResponseBaseDTO))]
        [SwaggerResponse((400), Type = typeof(ResponseBaseDTO))]
        [SwaggerResponse((404), Type = typeof(ResponseBaseDTO))]
        public IActionResult Put([FromBody] UserDTO userDTO)
        {
            try
            {
                _userService.Update<UserValidation>(userDTO);

                return ResponseOk(userDTO);
            }
            catch (ArgumentException argumentException)
            {
                return ResponseBadRequest(argumentException);
            }
            catch (Exception exception)
            {
                return ResponseNotFound(exception);
            }
        }

        [HttpDelete]
        [SwaggerResponse((204), Type = typeof(ResponseBaseDTO))]
        [SwaggerResponse((400), Type = typeof(ResponseBaseDTO))]
        [SwaggerResponse((404), Type = typeof(ResponseBaseDTO))]
        public IActionResult Delete([Required] int id)
        {
            try
            {
                _userService.Delete(id);

                return ResponseNoContent();
            }
            catch (ArgumentException argumentException)
            {
                return ResponseBadRequest(argumentException);
            }
            catch (Exception exception)
            {
                return ResponseNotFound(exception);
            }
        }

        [HttpGet("{id}")]
        [SwaggerResponse((200), Type = typeof(ResponseBaseDTO))]
        [SwaggerResponse((400), Type = typeof(ResponseBaseDTO))]
        [SwaggerResponse((404), Type = typeof(ResponseBaseDTO))]
        public IActionResult GetById([Required] int id)
        {
            try
            {
                return ResponseOk(_userService.FindById(id));
            }
            catch (ArgumentException argumentException)
            {
                return ResponseBadRequest(argumentException);
            }
            catch (Exception exception)
            {
                return ResponseNotFound(exception);
            }
        }

        [HttpGet]
        [SwaggerResponse((200), Type = typeof(ResponseBaseDTO))]
        [SwaggerResponse((400), Type = typeof(ResponseBaseDTO))]
        [SwaggerResponse((404), Type = typeof(ResponseBaseDTO))]
        public IActionResult GetAll()
        {
            try
            {
                return ResponseOk(_userService.FindAll());
            }
            catch (ArgumentException argumentException)
            {
                return ResponseBadRequest(argumentException);
            }
            catch (Exception exception)
            {
                return ResponseNotFound(exception);
            }
        }

        [HttpGet("EmailAddress/{emailAddress}/Password/{password}")]
        [SwaggerResponse((200), Type = typeof(ResponseBaseDTO))]
        [SwaggerResponse((204), Type = typeof(ResponseBaseDTO))]
        [SwaggerResponse((400), Type = typeof(ResponseBaseDTO))]
        [SwaggerResponse((404), Type = typeof(ResponseBaseDTO))]
        public IActionResult GetByEmailAddressAndPassword([Required] string emailAddress, [Required] string password)
        {
            try
            {
                var responseUserDTO = _userService.FindByEmailAddressAndPassword(emailAddress, password);

                if (responseUserDTO != null)
                    return ResponseOk(responseUserDTO);

                return ResponseNoContent();
            }
            catch (ArgumentException argumentException)
            {
                return ResponseBadRequest(argumentException);
            }
            catch (Exception exception)
            {
                return ResponseNotFound(exception);
            }
        }
    }
}