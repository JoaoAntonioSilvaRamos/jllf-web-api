﻿using JLLF.Domain.Entities;
using JLLF.Domain.Interfaces.Repositories;
using JLLF.Infra.Data.Context;
using JLLF.Infra.Data.Repositories.Base;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;

namespace JLLF.Infra.Data.Repositories
{
    public class SchedulingRepository : BaseRepository<Scheduling>, ISchedulingRepository
    {
        private readonly JLLFContext _context;

        public SchedulingRepository(JLLFContext context) : base(context)
        {
            _context = context;
        }

        public Scheduling FindById(int id)
        {
            return _context.Set<Scheduling>()
                .Where(s => s.Id == id)
                .Include(s => s.Collaborator)
                .Include(s => s.Customer)
                .Include(s => s.ServiceProvided)
                .OrderBy(s => s.Id)
                .FirstOrDefault();
        }

        public IList<Scheduling> FindAll()
        {
            return _context.Set<Scheduling>()
                .Where(s => s.Condition.Status == false)
                .Include(s => s.Collaborator)
                .Include(s => s.Customer)
                .Include(s => s.ServiceProvided)
                .OrderBy(s => s.Id)
                .ToList();
        }

        public IList<Scheduling> FindByPeriod(DateTime initialDate, DateTime finalDate)
        {
            return _context.Set<Scheduling>()
                .Where(s => s.Condition.Status == true && s.DateTime.Date >= initialDate.Date && s.DateTime.Date <= finalDate.Date)
                .Include(s => s.Collaborator)
                .Include(s => s.Customer)
                .Include(s => s.ServiceProvided)
                .OrderBy(s => s.Id)
                .ToList();
        }
    }
}