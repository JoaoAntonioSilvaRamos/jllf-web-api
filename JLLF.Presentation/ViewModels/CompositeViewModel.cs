﻿using System.Collections.Generic;

namespace JLLF.Presentation.ViewModels
{
    public class CompositeViewModel : SchedulingViewModel
    {
        public IList<ResponseCollaboratorViewModel> ResponseCollaboratorsViewModel { get; set; }
        public IList<ResponseCustomerViewModel> ResponseCustomersViewModel { get; set; }
        public IList<ResponseServiceProvidedViewModel> ResponseServicesProvidedViewModel { get; set; }
    }
}