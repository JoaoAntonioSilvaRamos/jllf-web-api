﻿using System;

namespace JLLF.Domain.VOs
{
    public class Person
    {
        public string Name { get; private set; }
        public string Gender { get; private set; }
        public string CPF { get; private set; }

        public Person(string name, string gender, string cpf)
        {
            ValidateEmptyName(name);
            ValidateNullName(name);
            ValidateNameLength(name);
            Name = name;
            ValidateEmptyGender(gender);
            ValidateNullGender(gender);
            ValidateGenderLength(gender);
            Gender = gender;
            ValidateEmptyCPF(cpf);
            ValidateNullCPF(cpf);
            ValidateCPFLength(cpf);
            CPF = cpf;
        }

        protected Person()
        {

        }

        private static void ValidateEmptyName(string name)
        {
            if (name == string.Empty)
                throw new ArgumentException("Name cannot be empty!");
        }

        private static void ValidateNullName(string name)
        {
            if (name == null)
                throw new ArgumentException("Name cannot be null!");
        }

        private static void ValidateNameLength(string name)
        {
            if (name.Length > 255)
                throw new ArgumentException("Name cannot be longer than 255 characters!");
        }

        private static void ValidateEmptyGender(string gender)
        {
            if (gender == string.Empty)
                throw new ArgumentException("Gender cannot be empty!");
        }

        private static void ValidateNullGender(string gender)
        {
            if (gender == null)
                throw new ArgumentException("Gender cannot be null!");
        }

        private static void ValidateGenderLength(string gender)
        {
            if (gender.Length > 9)
                throw new ArgumentException("Gender cannot be longer than 9 characters!");
        }

        private static void ValidateEmptyCPF(string cpf)
        {
            if (cpf == string.Empty)
                throw new ArgumentException("CPF cannot be empty!");
        }

        private static void ValidateNullCPF(string cpf)
        {
            if (cpf == null)
                throw new ArgumentException("CPF cannot be null!");
        }

        private static void ValidateCPFLength(string cpf)
        {
            if (cpf.Length != 14)
                throw new ArgumentException("CPF must contain 14 characters, in this format 000.000.000-00!");
        }
    }
}