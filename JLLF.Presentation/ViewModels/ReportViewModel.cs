﻿using System;
using System.ComponentModel.DataAnnotations;

namespace JLLF.Presentation.ViewModels
{
    public class ReportViewModel : CompositeViewModel
    {
        [Required(ErrorMessage = "Campo Obrigatório!")]
        public DateTime InitialDate { get; set; }

        [Required(ErrorMessage = "Campo Obrigatório!")]
        public DateTime FinalDate { get; set; }
    }
}