﻿using JLLF.Domain.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace JLLF.Infra.Data.EntitiesConfigurations
{
    public class CustomerEntityConfiguration : IEntityTypeConfiguration<Customer>
    {
        public void Configure(EntityTypeBuilder<Customer> entity)
        {
            entity.ToTable("Customers");

            entity.HasKey(c => c.Id);

            entity.OwnsOne(c => c.Person, person =>
            {
                person.Property(c => c.Name)
                    .IsRequired()
                    .HasColumnName("Name")
                    .HasColumnType($"Varchar({255})");

                person.Property(c => c.Gender)
                    .IsRequired()
                    .HasColumnName("Gender")
                    .HasColumnType($"Varchar({9})");

                person.Property(c => c.CPF)
                    .IsRequired()
                    .HasColumnName("CPF")
                    .HasColumnType($"Char({14})");
            });

            entity.OwnsOne(c => c.Telephone, telephone =>
            {
                telephone.Property(c => c.TelephoneNumber)
                    .IsRequired()
                    .HasColumnName("TelephoneNumber")
                    .HasColumnType($"Char({13})");
            });

            entity.OwnsOne(c => c.Email, email =>
            {
                email.Property(c => c.Address)
                    .IsRequired()
                    .HasColumnName("EmailAddress")
                    .HasColumnType($"Varchar({255})");
            });

            entity.OwnsOne(c => c.Registration, registration =>
            {
                registration.Property(c => c.RegistrationDate)
                    .IsRequired()
                    .HasColumnName("RegistrationDate")
                    .HasColumnType("DateTime");
            });

            entity.OwnsOne(c => c.Condition, condition =>
            {
                condition.Property(c => c.Status)
                    .IsRequired()
                    .HasColumnName("Status")
                    .HasColumnType("Boolean");
            });
        }
    }
}