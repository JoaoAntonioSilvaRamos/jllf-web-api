﻿using JLLF.Domain.Entities;
using JLLF.Domain.Interfaces.Repositories.Base;
using System;
using System.Collections.Generic;

namespace JLLF.Domain.Interfaces.Repositories
{
    public interface ISchedulingRepository : IBaseRepository<Scheduling>
    {
        Scheduling FindById(int id);
        IList<Scheduling> FindAll();
        IList<Scheduling> FindByPeriod(DateTime initialDate, DateTime finalDate);
    }
}