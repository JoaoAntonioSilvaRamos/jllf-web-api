﻿using System.Collections.Generic;

namespace JLLF.Domain.DTOs.Responses
{
    public class ResponseServiceProvidedDTO : ServiceProvidedDTO
    {
        public virtual ICollection<SchedulingDTO> SchedulingsDTOListByServiceProvidedDTO { get; set; }
    }
}