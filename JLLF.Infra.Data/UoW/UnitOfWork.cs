﻿using JLLF.Domain.Interfaces.UoW;
using JLLF.Infra.Data.Context;

namespace JLLF.Infra.Data.UoW
{
    public class UnitOfWork : IUnitOfWork
    {
        private readonly JLLFContext _context;

        public UnitOfWork(JLLFContext context)
        {
            _context = context;
        }

        public bool Commit()
        {
            return _context.SaveChanges() > 0;
        }
    }
}