﻿using FluentValidation;
using JLLF.Domain.DTOs;
using JLLF.Domain.DTOs.Responses;
using JLLF.Domain.Entities;
using JLLF.Domain.Interfaces.Repositories;
using JLLF.Domain.Interfaces.Services;
using JLLF.Domain.Interfaces.UoW;
using JLLF.Infra.CrossCutting.Constants;
using JLLF.Infra.CrossCutting.Messages;
using JLLF.Service.Mappings;
using JLLF.Service.Mappings.Responses;
using System;
using System.Collections.Generic;

namespace JLLF.Service.Services
{
    public class CollaboratorService : ICollaboratorService
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly ICollaboratorRepository _collaboratorRepository;
        private readonly CollaboratorMapping _collaboratorMapping = new();
        private readonly ResponseCollaboratorMapping _responseCollaboratorMapping = new();

        public CollaboratorService(IUnitOfWork unitOfWork, ICollaboratorRepository collaboratorRepository)
        {
            _unitOfWork = unitOfWork;
            _collaboratorRepository = collaboratorRepository;
        }

        public void Create<CollaboratorValidation>(CollaboratorDTO collaboratorDTO) where CollaboratorValidation : AbstractValidator<Collaborator>
        {
            var collaborator = _collaboratorMapping.Conversion(collaboratorDTO);

            if (collaborator.Id >= GeneralConstants.ONE)
                throw new Exception(GeneralMessages.IdMustBe_0.GetMessage("Collaborator"));
            
            Validate(collaborator, Activator.CreateInstance<CollaboratorValidation>());

            _collaboratorRepository.Insert(collaborator);

            _unitOfWork.Commit();
        }

        public void Update<CollaboratorValidation>(CollaboratorDTO collaboratorDTO) where CollaboratorValidation : AbstractValidator<Collaborator>
        {
            var collaborator = _collaboratorMapping.Conversion(collaboratorDTO);

            if (collaborator.Id <= GeneralConstants.ZERO)
                throw new Exception(GeneralMessages.IdMustBeGreatherThan_0.GetMessage("Collaborator"));

            Validate(collaborator, Activator.CreateInstance<CollaboratorValidation>());

            _collaboratorRepository.Update(collaborator);

            _unitOfWork.Commit();
        }

        public void Delete(int id)
        {
            if (id <= GeneralConstants.ZERO)
                throw new Exception(GeneralMessages.IdMustBeGreatherThan_0.GetMessage("Collaborator"));

            _collaboratorRepository.Delete(id);

            _unitOfWork.Commit();
        }

        public ResponseCollaboratorDTO FindById(int id)
        {
            if (id <= GeneralConstants.ZERO)
                throw new Exception(GeneralMessages.IdMustBeGreatherThan_0.GetMessage("Collaborator"));
           
            var collaboratorDB = _collaboratorRepository.FindById(id);

            var responseCollaboratorDTO = _responseCollaboratorMapping.Conversion(collaboratorDB);

            return responseCollaboratorDTO ?? throw new Exception(GeneralMessages.NotFound.GetMessage("Collaborator"));
        }

        public IList<ResponseCollaboratorDTO> FindAll()
        {
            var collaboratorsDB = _collaboratorRepository.FindAll();

            var responseCollaboratorsDTO = _responseCollaboratorMapping.ListConversion(collaboratorsDB);

            return responseCollaboratorsDTO != null && responseCollaboratorsDTO.Count > 0 ? responseCollaboratorsDTO : throw new Exception(GeneralMessages.NotFound.GetMessage("Collaborators"));
        }

        private static void Validate(Collaborator collaborator, AbstractValidator<Collaborator> validator)
        {
            if (collaborator == null)
                throw new Exception(GeneralMessages.NotFound.GetMessage("Collaborator"));

            validator.ValidateAndThrow(collaborator);
        }
    }
}