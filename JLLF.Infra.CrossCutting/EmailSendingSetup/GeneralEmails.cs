﻿using JLLF.Domain.DTOs;
using System.Net;
using System.Net.Mail;
using System.Text;

namespace JLLF.Infra.CrossCutting.EmailSendingSetup
{
    public class GeneralEmails
    {
        public static void SendEmail(GeneralEmailDTO generalEmailDTO)
        {
            MailMessage mailMessage = new("silvaramosjoaoantonio@gmail.com", generalEmailDTO.Receiver)
            {
                Subject = generalEmailDTO.Subject,
                IsBodyHtml = true,
                Body = generalEmailDTO.Body,
                SubjectEncoding = Encoding.GetEncoding("UTF-8"),
                BodyEncoding = Encoding.GetEncoding("UTF-8")
            };

            SmtpClient smtpClient = new("smtp.gmail.com", 587);

            smtpClient.UseDefaultCredentials = false;
            smtpClient.Credentials = new NetworkCredential("silvaramosjoaoantonio@gmail.com", "trator@BH180");
            smtpClient.EnableSsl = true;

            smtpClient.Send(mailMessage);
        }
    }
}