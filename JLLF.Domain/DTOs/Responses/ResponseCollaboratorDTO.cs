﻿using System.Collections.Generic;

namespace JLLF.Domain.DTOs.Responses
{
    public class ResponseCollaboratorDTO : CollaboratorDTO
    {
        public virtual ICollection<SchedulingDTO> SchedulingsDTOListByCollaboratorDTO { get; set; }
    }
}