﻿using System.Collections.Generic;

namespace JLLF.Domain.Interfaces.Repositories.Base
{
    public interface IBaseRepository<Entity> where Entity : class
    {
        void Insert(Entity entity);
        void Update(Entity entity);
        void Delete(int id);
        Entity SelectById(int id);
        IList<Entity> SelectAll();
    }
}