﻿using JLLF.Domain.Entities.Base;
using JLLF.Domain.VOs;
using System;

namespace JLLF.Domain.Entities
{
    public class Scheduling : BaseEntity
    {
        public int IdCollaborator { get; private set; }
        public virtual Collaborator Collaborator { get; private set; }
        public int IdCustomer { get; private set; }
        public virtual Customer Customer { get; private set; }
        public int IdServiceProvided { get; private set; }
        public virtual ServiceProvided ServiceProvided { get; private set; }
        public DateTime DateTime { get; private set; }
        public Condition Condition { get; private set; }

        public Scheduling(int? id, int idCollaborator, int idCustomer, int idServiceProvided, DateTime dateTime, bool? status)
        {
            Id = id;
            ValidateCollaboratorIDLessThanOrEqualToZero(idCollaborator);
            IdCollaborator = idCollaborator;
            ValidateCustomerIDLessThanOrEqualToZero(idCustomer);
            IdCustomer = idCustomer;
            ValidateServiceProvidedIDLessThanOrEqualToZero(idServiceProvided);
            IdServiceProvided = idServiceProvided;
            ValidateEmptyDateTime(dateTime);
            ValidateNullDateTime(dateTime);
            DateTime = dateTime;
            AssignCondition(status);
        }

        protected Scheduling()
        {

        }

        private static void ValidateCollaboratorIDLessThanOrEqualToZero(int idCollaborator)
        {
            if (idCollaborator <= 0)
                throw new ArgumentException("Collaborator id cannot be less than or equal to 0!");
        }

        private static void ValidateCustomerIDLessThanOrEqualToZero(int idCustomer)
        {
            if (idCustomer <= 0)
                throw new ArgumentException("Customer id cannot be less than or equal to 0!");
        }

        private static void ValidateServiceProvidedIDLessThanOrEqualToZero(int idServiceProvided)
        {
            if (idServiceProvided <= 0)
                throw new ArgumentException("Service provided id cannot be less than or equal to 0!");
        }

        private static void ValidateEmptyDateTime(DateTime dateTime)
        {
            if (dateTime.ToString() == string.Empty)
                throw new ArgumentException("Date and time cannot be empty!");
        }

        private static void ValidateNullDateTime(DateTime dateTime)
        {
            if (dateTime.ToString() == null)
                throw new ArgumentException("Date and time cannot be null!");
        }

        private bool AssignCondition(bool? status)
        {
            var condition = new Condition(status);

            Condition = condition;

            return true;
        }
    }
}