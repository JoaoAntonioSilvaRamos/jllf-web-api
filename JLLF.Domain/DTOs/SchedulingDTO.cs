﻿using System;

namespace JLLF.Domain.DTOs
{
    public class SchedulingDTO
    {
        public int? Id { get; set; }
        public int IdCollaboratorDTO { get; set; }
        public int IdCustomerDTO { get; set; }
        public int IdServiceProvidedDTO { get; set; }
        public DateTime DateTime { get; set; }
        public bool? Status { get; set; }
    }
}