﻿using JLLF.Domain.DTOs.Responses.Base;
using JLLF.Infra.CrossCutting.Extensions;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Net;

namespace JLLF.Application.Controllers.Base
{
    [ApiController]
    public abstract class BaseController : ControllerBase
    {
        protected IActionResult ResponseOk() =>
            Response(HttpStatusCode.OK);

        protected IActionResult ResponseOk(object data) =>
            Response(HttpStatusCode.OK, data);

        protected IActionResult ResponseCreated() =>
            Response(HttpStatusCode.Created);

        protected IActionResult ResponseCreated(object data) =>
            Response(HttpStatusCode.Created, data);

        protected IActionResult ResponseNoContent() =>
            Response(HttpStatusCode.NoContent);

        protected IActionResult ResponseNotModified() =>
            Response(HttpStatusCode.NotModified);

        protected IActionResult ResponseBadRequest() =>
            Response(HttpStatusCode.BadRequest, error: "A requisição é inválida!");

        protected IActionResult ResponseBadRequest(string error) =>
            Response(HttpStatusCode.BadRequest, error);

        protected IActionResult ResponseBadRequest(Exception exception) =>
            Response(HttpStatusCode.BadRequest, error: exception.Message);

        protected IActionResult ResponseNotFound() =>
            Response(HttpStatusCode.NotFound, error: "O recurso não foi encontrado!");

        protected IActionResult ResponseNotFound(IEnumerable<string> errors) =>
            Response(HttpStatusCode.NotFound, errors);

        protected IActionResult ResponseNotFound(Exception exception) =>
            Response(HttpStatusCode.NotFound, error: exception.Message);

        protected IActionResult ResponseUnauthorized() =>
            Response(HttpStatusCode.Unauthorized, error: "Permissão negada!");

        protected IActionResult ResponseUnauthorized(IEnumerable<string> errors) =>
            Response(HttpStatusCode.Unauthorized, errors);

        protected IActionResult ResponseInternalServerError() =>
            Response(HttpStatusCode.InternalServerError);

        protected IActionResult ResponseInternalServerError(IEnumerable<string> errors) =>
            Response(HttpStatusCode.InternalServerError, errors);

        protected IActionResult ResponseInternalServerError(Exception exception) =>
            Response(HttpStatusCode.InternalServerError, error: exception.Message);

        protected new JsonResult Response(HttpStatusCode httpStatusCode, object data, string errorMessage)
        {
            ResponseBaseDTO responseBaseDTO;

            if (string.IsNullOrWhiteSpace(errorMessage))
            {
                var success = httpStatusCode.IsSuccess();

                if (data != null)
                    responseBaseDTO = new ResponseBaseDTO(httpStatusCode, success, data);
                else
                    responseBaseDTO = new ResponseBaseDTO(httpStatusCode, success);
            }
            else
            {
                var errors = new List<string>();

                if (!string.IsNullOrWhiteSpace(errorMessage))
                    errors.Add(errorMessage);

                responseBaseDTO = new ResponseBaseDTO(httpStatusCode, false, errors);
            }

            return new JsonResult(responseBaseDTO)
            {
                StatusCode = (int)responseBaseDTO.HttpStatusCode
            };
        }

        protected new JsonResult Response(HttpStatusCode httpStatusCode, object data) =>
            Response(httpStatusCode, data, null);

        protected new JsonResult Response(HttpStatusCode httpStatusCode, string error) =>
            Response(httpStatusCode, null, error);

        protected new JsonResult Response(HttpStatusCode httpStatusCode) =>
            Response(httpStatusCode, null, null);
    }
}