﻿using System.Collections.Generic;

namespace JLLF.Domain.DTOs.Responses
{
    public class ResponseCompositeDTO : SchedulingDTO
    {
        public IList<ResponseCollaboratorDTO> ResponseCollaboratorsDTO { get; set; }
        public IList<ResponseCustomerDTO> ResponseCustomersDTO { get; set; }
        public IList<ResponseServiceProvidedDTO> ResponseServicesProvidedDTO { get; set; }
    }
}