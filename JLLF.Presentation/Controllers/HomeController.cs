﻿using JLLF.Domain.Interfaces.Services;
using JLLF.Presentation.Mappings;
using JLLF.Presentation.Mappings.Responses;
using JLLF.Presentation.ViewModels;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace JLLF.Presentation.Controllers
{
    public class HomeController : Controller
    {
        private readonly IUserService _userService;
        private readonly UserViewModelMapping userViewModelMapping = new();
        private readonly ResponseUserViewModelMapping responseUserViewModelMapping = new();

        public HomeController(IUserService userService)
        {
            _userService = userService;
        }

        [HttpGet]
        public IActionResult Index()
        {
            return View();
        }

        [HttpGet]
        public IActionResult SignIn(int? id)
        {
            if (id != null)
            {
                if (id == 0)
                {
                    HttpContext.Session.SetString("UsersEmailAddressLoggedIn", string.Empty);
                }
            }

            return View();
        }

        [HttpPost]
        public IActionResult SignIn(UserViewModel userViewModel)
        {
            if (ModelState.IsValid)
            {
                var userDTO = userViewModelMapping.Conversion(userViewModel);

                var responseUserDTO = _userService.FindByEmailAddressAndPassword(userDTO.EmailAddress, userDTO.Password);

                if (responseUserDTO != null)
                {
                    ResponseUserViewModel responseUserViewModel = responseUserViewModelMapping.Conversion(responseUserDTO);

                    HttpContext.Session.SetString("UsersEmailAddressLoggedIn", responseUserViewModel.EmailAddress);

                    return RedirectToAction("Index", "Schedulings");
                }
            }

            TempData["message"] = "E-mail ou Senha Incorretos!".ToString();

            ViewBag.message = TempData["message"] as string;

            return View();
        }
    }
}