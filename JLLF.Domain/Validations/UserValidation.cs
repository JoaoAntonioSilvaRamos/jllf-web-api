﻿using FluentValidation;
using JLLF.Domain.Entities;
using System;

namespace JLLF.Domain.Validations
{
    public class UserValidation : AbstractValidator<User>
    {
        public UserValidation()
        {
            RuleFor(u => u)
                .NotNull()
                .OnAnyFailure(u =>
                {
                    throw new ArgumentNullException("Invalid User!");
                });

            RuleFor(u => u.Email.Address)
                .NotEmpty().WithMessage("E-mail address cannot be empty!")
                .NotNull().WithMessage("E-mail address cannot be null!")
                .Length(1,255).WithMessage("E-mail address must be between 1 and 255 characters!");

            RuleFor(u => u.Password)
                .NotEmpty().WithMessage("Password cannot be empty!")
                .NotNull().WithMessage("Password cannot be null!")
                .Length(1,15).WithMessage("Password must be between 1 and 15 characters!");
        }
    }
}