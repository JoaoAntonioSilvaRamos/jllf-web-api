﻿namespace JLLF.Domain.VOs
{
    public class Condition
    {
        public bool? Status { get; private set; }

        public Condition(bool? status)
        {
            if (status == null)
                Status = true;

            Status = status;
        }

        protected Condition()
        {

        }
    }
}