﻿using JLLF.Domain.Entities;
using JLLF.Domain.Interfaces.Repositories.Base;
using System.Collections.Generic;

namespace JLLF.Domain.Interfaces.Repositories
{
    public interface ICollaboratorRepository : IBaseRepository<Collaborator>
    {
        Collaborator FindById(int id);
        IList<Collaborator> FindAll();
    }
}