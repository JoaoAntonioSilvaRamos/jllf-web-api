﻿using JLLF.Domain.DTOs;
using JLLF.Presentation.ViewModels;
using System;

namespace JLLF.Presentation.Mappings
{
    public class UserViewModelMapping
    {
        public UserDTO Conversion(UserViewModel userViewModel)
        {
            return new UserDTO
            {
                Id = 0,
                EmailAddress = userViewModel.EmailAddress,
                Password = userViewModel.Password,
                RegistrationDate = DateTime.Now,
                Status = true
            };
        }
    }
}