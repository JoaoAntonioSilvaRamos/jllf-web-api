﻿using FluentValidation;
using JLLF.Domain.Entities;
using System;

namespace JLLF.Domain.Validations
{
    public class CollaboratorValidation : AbstractValidator<Collaborator>
    {
        public CollaboratorValidation()
        {
            RuleFor(c => c)
                .NotNull()
                .OnAnyFailure(c =>
                {
                    throw new ArgumentNullException("Invalid Collaborator!");
                });

            RuleFor(c => c.Person.Name)
                .NotEmpty().WithMessage("Collaborator name cannot be empty!")
                .NotNull().WithMessage("Collaborator name cannot be null!")
                .Length(1,255).WithMessage("Collaborator's name must be between 1 and 255 characters!");

            RuleFor(c => c.Person.Gender)
                .NotEmpty().WithMessage("Collaborator gender cannot be empty!")
                .NotNull().WithMessage("Collaborator gender cannot be null!")
                .MinimumLength(8).WithMessage("Collaborator gender must be Feminino or Masculino!")
                .MaximumLength(9).WithMessage("Collaborator gender must be Feminino or Masculino!");

            RuleFor(c => c.Person.CPF)
                .NotEmpty().WithMessage("The collaborator CPF cannot be empty!")
                .NotNull().WithMessage("The collaborator CPF cannot be null!")
                .Length(14).WithMessage("The collaborator CPF must contain 14 characters, " +
                 "in this format 000.000.000-00");

            RuleFor(c => c.Telephone.TelephoneNumber)
                .NotEmpty().WithMessage("The mobile phone number of the collaborator can not be empty!")
                .NotNull().WithMessage("The mobile phone number of the collaborator can not be null!")
                .Length(13).WithMessage("The collaborator cell phone number must contain 13 characters, " +
                 "in this format 00 00000-0000");

            RuleFor(c => c.Function)
                .NotEmpty().WithMessage("Collaborator role cannot be empty!")
                .NotNull().WithMessage("Collaborator role cannot be null!")
                .Length(1,255).WithMessage("Collaborator role must be between 1 and 255 characters!");
        }
    }
}