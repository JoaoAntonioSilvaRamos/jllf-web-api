﻿using JLLF.Domain.DTOs;
using JLLF.Domain.Entities;

namespace JLLF.Service.Mappings
{
    public class ServiceProvidedMapping
    {
        public ServiceProvided Conversion(ServiceProvidedDTO serviceProvidedDTO)
        {
            return new ServiceProvided(serviceProvidedDTO.Id, serviceProvidedDTO.Description, serviceProvidedDTO.Price, serviceProvidedDTO.RegistrationDate, serviceProvidedDTO.Status);
        }

        public ServiceProvidedDTO Conversion(ServiceProvided serviceProvided)
        {
            return new ServiceProvidedDTO
            {
                Id = serviceProvided.Id,
                Description = serviceProvided.Description,
                Price = serviceProvided.Price,
                RegistrationDate = serviceProvided.Registration.RegistrationDate,
                Status = serviceProvided.Condition.Status
            };
        }
    }
}