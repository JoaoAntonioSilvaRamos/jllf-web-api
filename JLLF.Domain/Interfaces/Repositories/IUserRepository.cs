﻿using JLLF.Domain.Entities;
using JLLF.Domain.Interfaces.Repositories.Base;
using System.Collections.Generic;

namespace JLLF.Domain.Interfaces.Repositories
{
    public interface IUserRepository : IBaseRepository<User>
    {
        User FindById(int id);
        IList<User> FindAll();
        User FindByEmailAddressAndPassword(string emailAddress, string password);
    }
}