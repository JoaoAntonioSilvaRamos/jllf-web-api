﻿using JLLF.Domain.Entities;
using JLLF.Domain.Interfaces.Repositories;
using JLLF.Infra.Data.Context;
using JLLF.Infra.Data.Repositories.Base;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;

namespace JLLF.Infra.Data.Repositories
{
    public class CollaboratorRepository : BaseRepository<Collaborator>, ICollaboratorRepository
    {
        private readonly JLLFContext _context;

        public CollaboratorRepository(JLLFContext context) : base(context)
        {
            _context = context;
        }

        public Collaborator FindById(int id)
        {
            return _context.Set<Collaborator>()
                .Where(c => c.Id == id)
                .Include(c => c.SchedulingsListByCollaborator)
                .OrderBy(c => c.Id)
                .FirstOrDefault();
        }

        public IList<Collaborator> FindAll()
        {
            return _context.Set<Collaborator>()
                .Where(c => c.Condition.Status == true)
                .Include(c => c.SchedulingsListByCollaborator)
                .OrderBy(c => c.Id)
                .ToList();
        }
    }
}