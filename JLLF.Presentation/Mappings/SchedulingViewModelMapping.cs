﻿using JLLF.Domain.DTOs;
using JLLF.Presentation.ViewModels;
using System.Collections.Generic;
using System.Linq;

namespace JLLF.Presentation.Mappings
{
    public class SchedulingViewModelMapping
    {
        public SchedulingViewModel Conversion(SchedulingDTO schedulingDTO)
        {
            return new SchedulingViewModel
            {
                Id = schedulingDTO.Id,
                IdCollaboratorViewModel = schedulingDTO.IdCollaboratorDTO,
                IdCustomerViewModel = schedulingDTO.IdCustomerDTO,
                IdServiceProvidedViewModel = schedulingDTO.IdServiceProvidedDTO,
                DateTime = schedulingDTO.DateTime,
                Status = schedulingDTO.Status
            };
        }

        public ICollection<SchedulingViewModel> CollectionConversion(ICollection<SchedulingDTO> schedulingsDTO)
        {
            return schedulingsDTO.Select(item => Conversion(item)).ToList();
        }
    }
}