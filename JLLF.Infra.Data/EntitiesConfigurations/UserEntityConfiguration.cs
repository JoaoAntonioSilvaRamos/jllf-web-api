﻿using JLLF.Domain.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace JLLF.Infra.Data.EntitiesConfigurations
{
    public class UserEntityConfiguration : IEntityTypeConfiguration<User>
    {
        public void Configure(EntityTypeBuilder<User> entity)
        {
            entity.ToTable("Users");

            entity.HasKey(u => u.Id);

            entity.OwnsOne(u => u.Email, email =>
            {
                email.Property(u => u.Address)
                    .IsRequired()
                    .HasColumnName("EmailAddress")
                    .HasColumnType($"Varchar({255})");
            });

            entity.Property(u => u.Password)
                .IsRequired()
                .HasColumnName("Password")
                .HasColumnType($"Varchar({15})");

            entity.OwnsOne(c => c.Registration, registration =>
            {
                registration.Property(c => c.RegistrationDate)
                    .IsRequired()
                    .HasColumnName("RegistrationDate")
                    .HasColumnType("DateTime");
            });

            entity.OwnsOne(c => c.Condition, condition =>
            {
                condition.Property(c => c.Status)
                    .IsRequired()
                    .HasColumnName("Status")
                    .HasColumnType("Boolean");
            });
        }
    }
}