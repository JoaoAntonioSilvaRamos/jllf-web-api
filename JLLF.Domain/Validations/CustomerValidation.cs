﻿using FluentValidation;
using JLLF.Domain.Entities;
using System;

namespace JLLF.Domain.Validations
{
    public class CustomerValidation : AbstractValidator<Customer>
    {
        public CustomerValidation()
        {
            RuleFor(c => c)
                .NotNull()
                .OnAnyFailure(c =>
                {
                    throw new ArgumentNullException("Invalid Customer!");
                });

            RuleFor(c => c.Person.Name)
                .NotEmpty().WithMessage("Customer name cannot be empty!")
                .NotNull().WithMessage("Customer name cannot be null!")
                .Length(1,255).WithMessage("Customer's name must be between 1 and 255 characters!");

            RuleFor(c => c.Person.Gender)
                .NotEmpty().WithMessage("Customer gender cannot be empty!")
                .NotNull().WithMessage("Customer gender cannot be null!")
                .MinimumLength(8).WithMessage("Collaborator gender must be Feminino or Masculino!")
                .MaximumLength(9).WithMessage("Collaborator gender must be Feminino or Masculino!");

            RuleFor(c => c.Person.CPF)
                .NotEmpty().WithMessage("The customer CPF cannot be empty!")
                .NotNull().WithMessage("The customer CPF cannot be null!")
                .Length(14).WithMessage("The customer CPF must contain 14 characters, " +
                    "in this format 000.000.000-00");

            RuleFor(c => c.Telephone.TelephoneNumber)
                .NotEmpty().WithMessage("The mobile phone number of the customer can not be empty!")
                .NotNull().WithMessage("The mobile phone number of the customer can not be null!")
                .Length(13).WithMessage("The customer cell phone number must contain 13 characters, " +
                    "in this format 00 00000-0000");

            RuleFor(c => c.Email.Address)
                .NotEmpty().WithMessage("E-mail address cannot be empty!")
                .NotNull().WithMessage("E-mail address cannot be null!")
                .Length(1,255).WithMessage("E-mail address must be between 1 and 255 characters!");
        }
    }
}