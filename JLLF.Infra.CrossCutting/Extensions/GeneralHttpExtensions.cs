﻿using System.Net;
using System.Net.Http;

namespace JLLF.Infra.CrossCutting.Extensions
{
    public static class GeneralHttpExtensions
    {
        public static bool IsSuccess(this HttpStatusCode httpStatusCode) => new HttpResponseMessage(httpStatusCode).IsSuccessStatusCode;
    }
}