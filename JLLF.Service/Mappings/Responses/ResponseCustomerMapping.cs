﻿using JLLF.Domain.DTOs.Responses;
using JLLF.Domain.Entities;
using System.Collections.Generic;
using System.Linq;

namespace JLLF.Service.Mappings.Responses
{
    public class ResponseCustomerMapping
    {
        private SchedulingMapping schedulingMapping = new();

        public ResponseCustomerDTO Conversion(Customer customer)
        {
            return new ResponseCustomerDTO
            {
                Id = customer.Id,
                Name = customer.Person.Name,
                Gender = customer.Person.Gender,
                CPF = customer.Person.CPF,
                TelephoneNumber = customer.Telephone.TelephoneNumber,
                EmailAddress = customer.Email.Address,
                RegistrationDate = customer.Registration.RegistrationDate,
                Status = customer.Condition.Status,
                SchedulingsDTOListByCustomerDTO = schedulingMapping.CollectionConversion(customer.SchedulingsListByCustomer)
            };
        }

        public IList<ResponseCustomerDTO> ListConversion(IList<Customer> customers)
        {
            return customers.Select(item => Conversion(item)).ToList();
        }
    }
}