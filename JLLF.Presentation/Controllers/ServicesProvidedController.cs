﻿using JLLF.Domain.Interfaces.Services;
using JLLF.Domain.Validations;
using JLLF.Presentation.Mappings;
using JLLF.Presentation.Mappings.Responses;
using JLLF.Presentation.ViewModels;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;

namespace JLLF.Presentation.Controllers
{
    public class ServicesProvidedController : Controller
    {
        private readonly IServiceProvidedService _serviceProvidedService;
        private readonly ServiceProvidedViewModelMapping serviceProvidedViewModelMapping = new();
        private readonly ResponseServiceProvidedViewModelMapping responseServiceProvidedViewModelMapping = new();

        public ServicesProvidedController(IServiceProvidedService serviceProvidedService)
        {
            _serviceProvidedService = serviceProvidedService;
        }

        public IActionResult Index()
        {
            try
            {
                ViewBag.message = TempData["message"] as string;

                IList<ResponseServiceProvidedViewModel> responseServiceProvidedViewModels = responseServiceProvidedViewModelMapping.ListConversion(_serviceProvidedService.FindAll());

                if (responseServiceProvidedViewModels.Count > 0)
                    return View(responseServiceProvidedViewModels);

                return RedirectToAction("Create");
            }
            catch (Exception)
            {
                return RedirectToAction("Create");
            }
        }

        public IActionResult Create()
        {
            return View();
        }

        [HttpPost]
        public IActionResult Create(ServiceProvidedViewModel serviceProvidedViewModel)
        {
            if (ModelState.IsValid)
            {
                serviceProvidedViewModel.RegistrationDate = DateTime.Now;
                serviceProvidedViewModel.Status = true;

                _serviceProvidedService.Create<ServiceProvidedValidation>(serviceProvidedViewModelMapping.Conversion(serviceProvidedViewModel));

                TempData["message"] = "Serviço cadastrado com sucesso!".ToString();

                return RedirectToAction("Index");
            }

            return View();
        }

        public IActionResult Update(int id)
        {
            try
            {
                ResponseServiceProvidedViewModel responseServiceProvidedViewModel = responseServiceProvidedViewModelMapping.Conversion(_serviceProvidedService.FindById(id));

                if (responseServiceProvidedViewModel.Id > 0)
                    return View(responseServiceProvidedViewModel);

                return RedirectToAction("Index");
            }
            catch (Exception)
            {
                return RedirectToAction("Index");
            }
        }

        [HttpPost]
        public IActionResult Update(ServiceProvidedViewModel serviceProvidedViewModel)
        {
            if (ModelState.IsValid)
            {
                serviceProvidedViewModel.RegistrationDate = DateTime.Now;
                serviceProvidedViewModel.Status = true;

                _serviceProvidedService.Update<ServiceProvidedValidation>(serviceProvidedViewModelMapping.Conversion(serviceProvidedViewModel));

                TempData["message"] = "Serviço atualizado com sucesso!".ToString();

                return RedirectToAction("Index");
            }

            return View();
        }

        public IActionResult UpdateStatus(int id)
        {
            try
            {
                ResponseServiceProvidedViewModel responseServiceProvidedViewModel = responseServiceProvidedViewModelMapping.Conversion(_serviceProvidedService.FindById(id));

                if (responseServiceProvidedViewModel.Id > 0)
                    return View(responseServiceProvidedViewModel);

                return RedirectToAction("Index");
            }
            catch (Exception)
            {
                return RedirectToAction("Index");
            }
        }

        [HttpPost]
        public IActionResult UpdateStatus(ServiceProvidedViewModel serviceProvidedViewModel)
        {
            if (ModelState.IsValid)
            {
                serviceProvidedViewModel.RegistrationDate = DateTime.Now;
                serviceProvidedViewModel.Status = false;

                _serviceProvidedService.Update<ServiceProvidedValidation>(serviceProvidedViewModelMapping.Conversion(serviceProvidedViewModel));

                return RedirectToAction("Index");
            }

            return View();
        }

        public IActionResult Details(int id)
        {
            try
            {
                ResponseServiceProvidedViewModel responseServiceProvidedViewModel = responseServiceProvidedViewModelMapping.Conversion(_serviceProvidedService.FindById(id));

                if (responseServiceProvidedViewModel.Id > 0)
                    return View(responseServiceProvidedViewModel);

                return RedirectToAction("Index");
            }
            catch (Exception)
            {
                return RedirectToAction("Index");
            }
        }
    }
}