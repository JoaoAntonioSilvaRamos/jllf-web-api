﻿using System;

namespace JLLF.Domain.DTOs
{
    public class ServiceProvidedDTO
    {
        public int? Id { get; set; }
        public string Description { get; set; }
        public decimal Price { get; set; }
        public DateTime? RegistrationDate { get; set; }
        public bool? Status { get; set; }
    }
}