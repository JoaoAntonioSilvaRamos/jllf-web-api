﻿using JLLF.Domain.DTOs.Responses;
using JLLF.Domain.Entities;
using System.Collections.Generic;
using System.Linq;

namespace JLLF.Service.Mappings.Responses
{
    public class ResponseCollaboratorMapping
    {
        private SchedulingMapping schedulingMapping = new();

        public ResponseCollaboratorDTO Conversion(Collaborator collaborator)
        {
            return new ResponseCollaboratorDTO
            {
                Id = collaborator.Id,
                Name = collaborator.Person.Name,
                Gender = collaborator.Person.Gender,
                CPF = collaborator.Person.CPF,
                TelephoneNumber = collaborator.Telephone.TelephoneNumber,
                Function = collaborator.Function,
                RegistrationDate = collaborator.Registration.RegistrationDate,
                Status = collaborator.Condition.Status,
                SchedulingsDTOListByCollaboratorDTO = schedulingMapping.CollectionConversion(collaborator.SchedulingsListByCollaborator)
            };
        }

        public IList<ResponseCollaboratorDTO> ListConversion(IList<Collaborator> collaborators)
        {
            return collaborators.Select(item => Conversion(item)).ToList();
        }
    }
}