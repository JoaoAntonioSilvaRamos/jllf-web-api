using JLLF.Domain.Interfaces.Repositories;
using JLLF.Domain.Interfaces.Services;
using JLLF.Domain.Interfaces.UoW;
using JLLF.Infra.Data.Context;
using JLLF.Infra.Data.Repositories;
using JLLF.Infra.Data.UoW;
using JLLF.Service.Services;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using System;

namespace JLLF.Presentation
{
    public class Startup
    {
        public IConfiguration _configuration { get; }

        public Startup(IConfiguration configuration)
        {
            _configuration = configuration;
        }

        public void ConfigureServices(IServiceCollection services)
        {
            var connectionString = _configuration["MySqlConnection:MySqlConnectionString"];

            services.AddDbContext<JLLFContext>(options => options.UseMySql(connectionString, new MySqlServerVersion(new Version(8, 0, 25))));

            services.AddScoped<IUnitOfWork, UnitOfWork>();

            services.AddTransient<ICollaboratorService, CollaboratorService>();
            services.AddScoped<ICollaboratorRepository, CollaboratorRepository>();

            services.AddTransient<ICustomerService, CustomerService>();
            services.AddScoped<ICustomerRepository, CustomerRepository>();

            services.AddTransient<ISchedulingService, SchedulingService>();
            services.AddScoped<ISchedulingRepository, SchedulingRepository>();

            services.AddTransient<IServiceProvidedService, ServiceProvidedService>();
            services.AddScoped<IServiceProvidedRepository, ServiceProvidedRepository>();

            services.AddTransient<IUserService, UserService>();
            services.AddScoped<IUserRepository, UserRepository>();

            services.AddMvc();

            services.AddSession();

            services.AddSingleton<IHttpContextAccessor, HttpContextAccessor>();

            services.AddControllersWithViews();
        }

        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler("/Home/Error");

                app.UseHsts();
            }

            app.UseHttpsRedirection();

            app.UseStaticFiles();

            app.UseSession();

            app.UseRouting();

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllerRoute(name: "default", pattern: "{controller=Home}/{action=SignIn}/{id?}");
            });
        }
    }
}