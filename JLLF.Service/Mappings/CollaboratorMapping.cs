﻿using JLLF.Domain.DTOs;
using JLLF.Domain.Entities;

namespace JLLF.Service.Mappings
{
    public class CollaboratorMapping
    {
        public Collaborator Conversion(CollaboratorDTO collaboratorDTO)
        {
            return new Collaborator(collaboratorDTO.Id, collaboratorDTO.Name, collaboratorDTO.Gender, collaboratorDTO.CPF, collaboratorDTO.TelephoneNumber, collaboratorDTO.Function, collaboratorDTO.RegistrationDate, collaboratorDTO.Status);
        }

        public CollaboratorDTO Conversion(Collaborator collaborator)
        {
            return new CollaboratorDTO
            {
                Id = collaborator.Id,
                Name = collaborator.Person.Name,
                Gender = collaborator.Person.Gender,
                CPF = collaborator.Person.CPF,
                TelephoneNumber = collaborator.Telephone.TelephoneNumber,
                Function = collaborator.Function,
                RegistrationDate = collaborator.Registration.RegistrationDate,
                Status = collaborator.Condition.Status
            };
        }
    }
}