﻿using JLLF.Domain.DTOs.Responses;
using JLLF.Domain.Entities;
using System.Collections.Generic;
using System.Linq;

namespace JLLF.Service.Mappings.Responses
{
    public class ResponseSchedulingMapping
    {
        private CollaboratorMapping collaboratorMapping = new();
        private CustomerMapping customerMapping = new();
        private ServiceProvidedMapping serviceProvidedMapping = new();

        public ResponseSchedulingDTO Conversion(Scheduling scheduling)
        {
            return new ResponseSchedulingDTO
            {
                Id = scheduling.Id,
                IdCollaboratorDTO = scheduling.IdCollaborator,
                CollaboratorDTO = collaboratorMapping.Conversion(scheduling.Collaborator),
                IdCustomerDTO = scheduling.IdCustomer,
                CustomerDTO = customerMapping.Conversion(scheduling.Customer),
                IdServiceProvidedDTO = scheduling.IdServiceProvided,
                ServiceProvidedDTO = serviceProvidedMapping.Conversion(scheduling.ServiceProvided),
                DateTime = scheduling.DateTime,
                Status = scheduling.Condition.Status
            };
        }

        public IList<ResponseSchedulingDTO> ListConversion(IList<Scheduling> schedulings)
        {
            return schedulings.Select(item => Conversion(item)).ToList();
        }
    }
}