﻿using System;

namespace JLLF.Domain.VOs
{
    public class Registration
    {
        public DateTime? RegistrationDate { get; private set; }

        public Registration(DateTime? registrationDate)
        {
            if (registrationDate == null)
                RegistrationDate = DateTime.Now;

            RegistrationDate = registrationDate;
        }

        protected Registration()
        {

        }
    }
}