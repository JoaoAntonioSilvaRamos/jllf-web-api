﻿using FluentValidation;
using JLLF.Domain.DTOs;
using JLLF.Domain.DTOs.Responses;
using JLLF.Domain.Entities;
using System.Collections.Generic;

namespace JLLF.Domain.Interfaces.Services
{
    public interface IUserService
    {
        void Create<UserValidation>(UserDTO userDTO) where UserValidation : AbstractValidator<User>;
        void Update<UserValidation>(UserDTO userDTO) where UserValidation : AbstractValidator<User>;
        void Delete(int id);
        ResponseUserDTO FindById(int id);
        IList<ResponseUserDTO> FindAll();
        ResponseUserDTO FindByEmailAddressAndPassword(string emailAddress, string password);
    }
}