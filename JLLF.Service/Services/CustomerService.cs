﻿using FluentValidation;
using JLLF.Domain.DTOs;
using JLLF.Domain.DTOs.Responses;
using JLLF.Domain.Entities;
using JLLF.Domain.Interfaces.Repositories;
using JLLF.Domain.Interfaces.Services;
using JLLF.Domain.Interfaces.UoW;
using JLLF.Infra.CrossCutting.Constants;
using JLLF.Infra.CrossCutting.Messages;
using JLLF.Service.Mappings;
using JLLF.Service.Mappings.Responses;
using System;
using System.Collections.Generic;

namespace JLLF.Service.Services
{
    public class CustomerService : ICustomerService
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly ICustomerRepository _customerRepository;
        private readonly CustomerMapping _customerMapping = new();
        private readonly ResponseCustomerMapping _responseCustomerMapping = new();

        public CustomerService(IUnitOfWork unitOfWork, ICustomerRepository customerRepository)
        {
            _unitOfWork = unitOfWork;
            _customerRepository = customerRepository;
        }

        public void Create<CustomerValidation>(CustomerDTO customerDTO) where CustomerValidation : AbstractValidator<Customer>
        {
            var customer = _customerMapping.Conversion(customerDTO);

            if (customer.Id >= GeneralConstants.ONE)
                throw new Exception(GeneralMessages.IdMustBe_0.GetMessage("Customer"));
            
            Validate(customer, Activator.CreateInstance<CustomerValidation>());

            _customerRepository.Insert(customer);

            _unitOfWork.Commit();
        }

        public void Update<CustomerValidation>(CustomerDTO customerDTO) where CustomerValidation : AbstractValidator<Customer>
        {
            var customer = _customerMapping.Conversion(customerDTO);

            if (customer.Id <= GeneralConstants.ZERO)
                throw new Exception(GeneralMessages.IdMustBeGreatherThan_0.GetMessage("Customer"));

            Validate(customer, Activator.CreateInstance<CustomerValidation>());

            _customerRepository.Update(customer);

            _unitOfWork.Commit();
        }

        public void Delete(int id)
        {
            if (id <= GeneralConstants.ZERO)
                throw new Exception(GeneralMessages.IdMustBeGreatherThan_0.GetMessage("Customer"));

            _customerRepository.Delete(id);

            _unitOfWork.Commit();
        }

        public ResponseCustomerDTO FindById(int id)
        {
            if (id <= GeneralConstants.ZERO)
                throw new Exception(GeneralMessages.IdMustBeGreatherThan_0.GetMessage("Customer"));

            var customerDB = _customerRepository.FindById(id);

            var responseCustomerDTO = _responseCustomerMapping.Conversion(customerDB);

            return responseCustomerDTO ?? throw new Exception(GeneralMessages.NotFound.GetMessage("Customer"));
        }

        public IList<ResponseCustomerDTO> FindAll()
        {
            var customersDB = _customerRepository.FindAll();

            var responseCustomersDTO = _responseCustomerMapping.ListConversion(customersDB);

            return responseCustomersDTO != null && responseCustomersDTO.Count > 0 ? responseCustomersDTO : throw new Exception(GeneralMessages.NotFound.GetMessage("Customers"));
        }

        private static void Validate(Customer customer, AbstractValidator<Customer> validator)
        {
            if (customer == null)
                throw new Exception(GeneralMessages.NotFound.GetMessage("Customer"));

            validator.ValidateAndThrow(customer);
        }
    }
}