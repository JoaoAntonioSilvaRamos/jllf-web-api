﻿using JLLF.Domain.Interfaces.Services;
using JLLF.Presentation.Mappings.Responses;
using JLLF.Presentation.ViewModels;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;

namespace JLLF.Presentation.Controllers
{
    public class ReportsController : Controller
    {
        private readonly ISchedulingService _schedulingService;
        private readonly ResponseSchedulingViewModelMapping responseSchedulingViewModelMapping = new();
        
        public ReportsController(ISchedulingService schedulingService)
        {
            _schedulingService = schedulingService;
        }

        public IActionResult Index()
        {
            ViewBag.message = TempData["message"] as string;

            return View();
        }

        public IActionResult ReadByPeriod()
        {
            ViewBag.message = TempData["message"] as string;

            return View();
        }

        public IActionResult ReportByPeriod(ReportViewModel reportViewModel)
        {
            try
            {
                IList<ResponseSchedulingViewModel> responseSchedulingViewModels = responseSchedulingViewModelMapping.ListConversion(_schedulingService.FindByPeriod(reportViewModel.InitialDate, reportViewModel.FinalDate));

                if (responseSchedulingViewModels.Count > 0)
                    return View(responseSchedulingViewModels);

                TempData["message"] = "Não há agendamentos entre as datas e horários informados!".ToString();

                return RedirectToAction("Index");
            }
            catch (Exception)
            {
                return RedirectToAction("Index");
            }
        }
    }
}