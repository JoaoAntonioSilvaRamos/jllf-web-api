﻿using JLLF.Domain.Entities;
using JLLF.Domain.Interfaces.Repositories;
using JLLF.Infra.Data.Context;
using JLLF.Infra.Data.Repositories.Base;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;

namespace JLLF.Infra.Data.Repositories
{
    public class ServiceProvidedRepository : BaseRepository<ServiceProvided>, IServiceProvidedRepository
    {
        private readonly JLLFContext _context;

        public ServiceProvidedRepository(JLLFContext context) : base(context)
        {
            _context = context;
        }

        public ServiceProvided FindById(int id)
        {
            return _context.Set<ServiceProvided>()
                .Where(sp => sp.Id == id)
                .Include(sp => sp.SchedulingsListByServiceProvided)
                .OrderBy(sp => sp.Id)
                .FirstOrDefault();
        }

        public IList<ServiceProvided> FindAll()
        {
            return _context.Set<ServiceProvided>()
                .Where(sp => sp.Condition.Status == true)
                .Include(sp => sp.SchedulingsListByServiceProvided)
                .OrderBy(sp => sp.Id)
                .ToList();
        }
    }
}