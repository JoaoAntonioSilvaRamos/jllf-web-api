﻿using JLLF.Domain.DTOs;
using JLLF.Domain.DTOs.Responses;
using JLLF.Presentation.Mappings.Responses;
using JLLF.Presentation.ViewModels;

namespace JLLF.Presentation.Mappings
{
    public class CompositeViewModelMapping
    {
        private readonly ResponseCollaboratorViewModelMapping responseCollaboratorViewModelMapping = new();
        private readonly ResponseCustomerViewModelMapping responseCustomerViewModelMapping = new();
        private readonly ResponseServiceProvidedViewModelMapping responseServiceProvidedViewModelMapping = new();

        public SchedulingDTO Conversion(CompositeViewModel compositeViewModel)
        {
            return new SchedulingDTO
            {
                Id = compositeViewModel.Id,
                IdCollaboratorDTO = compositeViewModel.IdCollaboratorViewModel,
                IdCustomerDTO = compositeViewModel.IdCustomerViewModel,
                IdServiceProvidedDTO = compositeViewModel.IdServiceProvidedViewModel,
                DateTime = compositeViewModel.DateTime,
                Status = compositeViewModel.Status
            };
        }

        public CompositeViewModel Conversion(ResponseCompositeDTO responseCompositeDTO)
        {
            return new CompositeViewModel
            {
                ResponseCollaboratorsViewModel = responseCollaboratorViewModelMapping.ListConversion(responseCompositeDTO.ResponseCollaboratorsDTO),
                ResponseCustomersViewModel = responseCustomerViewModelMapping.ListConversion(responseCompositeDTO.ResponseCustomersDTO),
                ResponseServicesProvidedViewModel = responseServiceProvidedViewModelMapping.ListConversion(responseCompositeDTO.ResponseServicesProvidedDTO)
            };
        }

        public CompositeViewModel ConversionAll(ResponseCompositeDTO responseCompositeDTO)
        {
            return new CompositeViewModel
            {
                Id = responseCompositeDTO.Id,
                IdCollaboratorViewModel = responseCompositeDTO.IdCollaboratorDTO,
                IdCustomerViewModel = responseCompositeDTO.IdCustomerDTO,
                IdServiceProvidedViewModel = responseCompositeDTO.IdServiceProvidedDTO,
                DateTime = responseCompositeDTO.DateTime,
                ResponseCollaboratorsViewModel = responseCollaboratorViewModelMapping.ListConversion(responseCompositeDTO.ResponseCollaboratorsDTO),
                ResponseCustomersViewModel = responseCustomerViewModelMapping.ListConversion(responseCompositeDTO.ResponseCustomersDTO),
                ResponseServicesProvidedViewModel = responseServiceProvidedViewModelMapping.ListConversion(responseCompositeDTO.ResponseServicesProvidedDTO)
            };
        }
    }
}