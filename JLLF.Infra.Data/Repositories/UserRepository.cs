﻿using JLLF.Domain.Entities;
using JLLF.Domain.Interfaces.Repositories;
using JLLF.Infra.Data.Context;
using JLLF.Infra.Data.Repositories.Base;
using System.Collections.Generic;
using System.Linq;

namespace JLLF.Infra.Data.Repositories
{
    public class UserRepository : BaseRepository<User>, IUserRepository
    {
        private readonly JLLFContext _context;
    
        public UserRepository(JLLFContext context) : base(context)
        {
            _context = context;
        }

        public User FindById(int id)
        {
            return _context.Set<User>()
                .Where(c => c.Id == id)
                .OrderBy(c => c.Id)
                .FirstOrDefault();
        }

        public IList<User> FindAll()
        {
            return _context.Set<User>()
                .Where(c => c.Condition.Status == true)
                .OrderBy(c => c.Id)
                .ToList();
        }

        public User FindByEmailAddressAndPassword(string emailAddress, string password)
        {
            var user = _context.Set<User>()
                .Where(u => u.Email.Address == emailAddress && u.Password == password)
                .FirstOrDefault();

            return user ?? null;
        }
    }
}