﻿using JLLF.Domain.Entities.Base;
using JLLF.Domain.VOs;
using System;
using System.Collections.Generic;

namespace JLLF.Domain.Entities
{
    public class Customer : BaseEntity
    {
        public Person Person { get; private set; }
        public Telephone Telephone { get; private set; }
        public Email Email { get; private set; }
        public Registration Registration { get; private set; }
        public Condition Condition { get; private set; }
        public virtual ICollection<Scheduling> SchedulingsListByCustomer { get; private set; }

        public Customer(int? id, string name, string gender, string cpf, string telephoneNumber, string emailAddress, DateTime? registrationDate, bool? status)
        {
            Id = id;
            AssignPerson(name, gender, cpf);
            AssignTelephone(telephoneNumber);
            AssignEmail(emailAddress);
            AssignRegistration(registrationDate);
            AssignCondition(status);
        }

        protected Customer()
        {

        }

        private bool AssignPerson(string name, string gender, string cpf)
        {
            var person = new Person(name, gender, cpf);

            Person = person;

            return true;
        }

        private bool AssignTelephone(string telephoneNumber)
        {
            var telephone = new Telephone(telephoneNumber);

            Telephone = telephone;

            return true;
        }

        private bool AssignEmail(string emailAddress)
        {
            var email = new Email(emailAddress);

            Email = email;

            return true;
        }

        private bool AssignRegistration(DateTime? registrationDate)
        {
            var registration = new Registration(registrationDate);

            Registration = registration;

            return true;
        }

        private bool AssignCondition(bool? status)
        {
            var condition = new Condition(status);

            Condition = condition;

            return true;
        }
    }
}