﻿using System;
using System.ComponentModel.DataAnnotations;

namespace JLLF.Presentation.ViewModels
{
    public class UserViewModel
    {
        public int? Id { get; set; }

        [Required(ErrorMessage = "Campo Obrigatório!")]
        [MinLength(1, ErrorMessage = "Campo Deve Conter No Mínimo 1 Caracter!")]
        [MaxLength(255, ErrorMessage = "Campo Deve Conter No Máximo 255 Caracteres!")]
        [DataType(DataType.EmailAddress)]
        [EmailAddress(ErrorMessage = "E-mail Em Formato Inválido!")]
        public string EmailAddress { get; set; }

        [Required(ErrorMessage = "Campo Obrigatório!")]
        [MinLength(1, ErrorMessage = "Campo Deve Conter No Mínimo 1 Caracter!")]
        [MaxLength(15, ErrorMessage = "Campo Deve Conter No Máximo 15 Caracteres!")]
        public string Password { get; set; }

        public DateTime? RegistrationDate { get; set; }

        public bool? Status { get; set; }
    }

    public class ResponseUserViewModel : UserViewModel
    {
        
    }
}