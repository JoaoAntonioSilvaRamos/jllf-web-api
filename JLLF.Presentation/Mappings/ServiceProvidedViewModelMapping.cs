﻿using JLLF.Domain.DTOs;
using JLLF.Presentation.ViewModels;

namespace JLLF.Presentation.Mappings
{
    public class ServiceProvidedViewModelMapping
    {
        public ServiceProvidedDTO Conversion(ServiceProvidedViewModel serviceProvidedViewModel)
        {
            return new ServiceProvidedDTO
            {
                Id = serviceProvidedViewModel.Id,
                Description = serviceProvidedViewModel.Description,
                Price = serviceProvidedViewModel.Price,
                RegistrationDate = serviceProvidedViewModel.RegistrationDate,
                Status = serviceProvidedViewModel.Status
            };
        }

        public ServiceProvidedViewModel Conversion(ServiceProvidedDTO serviceProvidedDTO)
        {
            return new ServiceProvidedViewModel
            {
                Id = serviceProvidedDTO.Id,
                Description = serviceProvidedDTO.Description,
                Price = serviceProvidedDTO.Price,
                RegistrationDate = serviceProvidedDTO.RegistrationDate,
                Status = serviceProvidedDTO.Status
            };
        }
    }
}