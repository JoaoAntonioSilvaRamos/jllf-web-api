﻿using JLLF.Domain.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace JLLF.Infra.Data.EntitiesConfigurations
{
    public class ServiceProvidedEntityConfiguration : IEntityTypeConfiguration<ServiceProvided>
    {
        public void Configure(EntityTypeBuilder<ServiceProvided> entity)
        {
            entity.ToTable("ServicesProvided");

            entity.HasKey(sp => sp.Id);

            entity.Property(sp => sp.Description)
                .IsRequired()
                .HasColumnName("Description")
                .HasColumnType($"Varchar({255})");

            entity.Property(sp => sp.Price)
                .IsRequired()
                .HasColumnName("Price")
                .HasColumnType("Decimal(9,2)");

            entity.OwnsOne(c => c.Registration, registration =>
            {
                registration.Property(c => c.RegistrationDate)
                    .IsRequired()
                    .HasColumnName("RegistrationDate")
                    .HasColumnType("DateTime");
            });

            entity.OwnsOne(c => c.Condition, condition =>
            {
                condition.Property(c => c.Status)
                    .IsRequired()
                    .HasColumnName("Status")
                    .HasColumnType("Boolean");
            });
        }
    }
}