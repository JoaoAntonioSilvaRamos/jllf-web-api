﻿namespace JLLF.Domain.Interfaces.UoW
{
    public interface IUnitOfWork
    {
        bool Commit();
    }
}