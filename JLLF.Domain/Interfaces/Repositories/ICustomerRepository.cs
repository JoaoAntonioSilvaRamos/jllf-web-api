﻿using JLLF.Domain.Entities;
using JLLF.Domain.Interfaces.Repositories.Base;
using System.Collections.Generic;

namespace JLLF.Domain.Interfaces.Repositories
{
    public interface ICustomerRepository : IBaseRepository<Customer>
    {
        Customer FindById(int id);
        IList<Customer> FindAll();
    }
}