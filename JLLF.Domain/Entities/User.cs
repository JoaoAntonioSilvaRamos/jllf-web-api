﻿using JLLF.Domain.Entities.Base;
using JLLF.Domain.VOs;
using System;

namespace JLLF.Domain.Entities
{
    public class User : BaseEntity
    {
        public Email Email { get; private set; }
        public string Password { get; private set; }
        public Registration Registration { get; private set; }
        public Condition Condition { get; private set; }

        public User(int? id, string emailAddress, string password, DateTime? registrationDate, bool? status)
        {
            Id = id;
            AssignEmail(emailAddress);
            ValidateEmptyPassword(password);
            ValidateNullPassword(password);
            ValidatePasswordLength(password);
            Password = password;
            AssignRegistration(registrationDate);
            AssignCondition(status);
        }

        protected User()
        {

        }

        private bool AssignEmail(string emailAddress)
        {
            var email = new Email(emailAddress);

            Email = email;

            return true;
        }

        private static void ValidateEmptyPassword(string password)
        {
            if (password == string.Empty)
                throw new ArgumentException("Password cannot be empty!");
        }

        private static void ValidateNullPassword(string password)
        {
            if (password == null)
                throw new ArgumentException("Password cannot be null!");
        }

        private static void ValidatePasswordLength(string password)
        {
            if (password.Length > 15)
                throw new ArgumentException("Password cannot be longer than 15 characters!");
        }

        private bool AssignRegistration(DateTime? registrationDate)
        {
            var registration = new Registration(registrationDate);

            Registration = registration;

            return true;
        }

        private bool AssignCondition(bool? status)
        {
            var condition = new Condition(status);

            Condition = condition;

            return true;
        }
    }
}