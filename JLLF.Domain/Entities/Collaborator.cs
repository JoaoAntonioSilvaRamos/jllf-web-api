﻿using JLLF.Domain.Entities.Base;
using JLLF.Domain.VOs;
using System;
using System.Collections.Generic;

namespace JLLF.Domain.Entities
{
    public class Collaborator : BaseEntity
    {
        public Person Person { get; private set; }
        public Telephone Telephone { get; private set; }
        public string Function { get; private set; }
        public Registration Registration { get; private set; }
        public Condition Condition { get; private set; }
        public virtual ICollection<Scheduling> SchedulingsListByCollaborator { get; private set; }

        public Collaborator(int? id, string name, string gender, string cpf, string telephoneNumber, string function, DateTime? registrationDate, bool? status)
        {
            Id = id;
            AssignPerson(name, gender, cpf);
            AssignTelephone(telephoneNumber);
            ValidateEmptyFunction(function);
            ValidateNullFunction(function);
            ValidateFunctionLength(function);
            Function = function;
            AssignRegistration(registrationDate);
            AssignCondition(status);
        }

        protected Collaborator()
        {

        }

        private bool AssignPerson(string name, string gender, string cpf)
        {
            var person = new Person(name, gender, cpf);

            Person = person;

            return true;
        }

        private bool AssignTelephone(string telephoneNumber)
        {
            var telephone = new Telephone(telephoneNumber);

            Telephone = telephone;

            return true;
        }

        private static void ValidateEmptyFunction(string function)
        {
            if (function == string.Empty)
                throw new ArgumentException("Function cannot be empty!");
        }

        private static void ValidateNullFunction(string function)
        {
            if (function == null)
                throw new ArgumentException("Function cannot be null!");
        }

        private static void ValidateFunctionLength(string function)
        {
            if (function.Length > 255)
                throw new ArgumentException("Function cannot be longer than 255 characters!");
        }

        private bool AssignRegistration(DateTime? registrationDate)
        {
            var registration = new Registration(registrationDate);

            Registration = registration;

            return true;
        }

        private bool AssignCondition(bool? status)
        {
            var condition = new Condition(status);

            Condition = condition;

            return true;
        }
    }
}