﻿using FluentValidation;
using JLLF.Domain.DTOs;
using JLLF.Domain.DTOs.Responses;
using JLLF.Domain.Entities;
using System.Collections.Generic;

namespace JLLF.Domain.Interfaces.Services
{
    public interface ICollaboratorService
    {
        void Create<CollaboratorValidation>(CollaboratorDTO collaboratorDTO) where CollaboratorValidation : AbstractValidator<Collaborator>;
        void Update<CollaboratorValidation>(CollaboratorDTO collaboratorDTO) where CollaboratorValidation : AbstractValidator<Collaborator>;
        void Delete(int id);
        ResponseCollaboratorDTO FindById(int id);
        IList<ResponseCollaboratorDTO> FindAll();
    }
}