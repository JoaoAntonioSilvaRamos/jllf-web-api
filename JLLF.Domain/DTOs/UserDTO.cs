﻿using System;

namespace JLLF.Domain.DTOs
{
    public class UserDTO
    {
        public int? Id { get; set; }
        public string EmailAddress { get; set; }
        public string Password { get; set; }
        public DateTime? RegistrationDate { get; set; }
        public bool? Status { get; set; }
    }
}