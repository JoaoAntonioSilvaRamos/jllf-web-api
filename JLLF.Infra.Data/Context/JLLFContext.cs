﻿using JLLF.Domain.Entities;
using JLLF.Infra.Data.EntitiesConfigurations;
using Microsoft.EntityFrameworkCore;
using System;

namespace JLLF.Infra.Data.Context
{
    public class JLLFContext : DbContext
    {
        public JLLFContext()
        {
            
        }

        public JLLFContext(DbContextOptions<JLLFContext> options) : base(options)
        {

        }

        public DbSet<Collaborator> Collaborators { get; set; }
        public DbSet<Customer> Customers { get; set; }
        public DbSet<Scheduling> Schedulings { get; set; }
        public DbSet<ServiceProvided> ServicesProvided { get; set; }
        public DbSet<User> Users { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
                optionsBuilder.UseMySql("Server=localhost;Port=3306;DataBase=JLLF;Uid=root;Pwd=;SslMode=none;", new MySqlServerVersion(new Version(8, 0, 25)));
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.Entity<Collaborator>(new CollaboratorEntityConfiguration().Configure);
            modelBuilder.Entity<Customer>(new CustomerEntityConfiguration().Configure);
            modelBuilder.Entity<Scheduling>(new SchedulingEntityConfiguration().Configure);
            modelBuilder.Entity<ServiceProvided>(new ServiceProvidedEntityConfiguration().Configure);
            modelBuilder.Entity<User>(new UserEntityConfiguration().Configure);
        }
    }
}