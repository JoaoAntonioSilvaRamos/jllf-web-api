﻿using System.Collections.Generic;
using System.Net;

namespace JLLF.Domain.DTOs.Responses.Base
{
    public class ResponseBaseDTO
    {
        public HttpStatusCode HttpStatusCode { get; private set; }
        public bool Success { get; private set; }
        public object Data { get; private set; }
        public IEnumerable<string> Errors { get; private set; }

        public ResponseBaseDTO(HttpStatusCode httpStatusCode, bool success)
        {
            HttpStatusCode = httpStatusCode;
            Success = success;
        }

        public ResponseBaseDTO(HttpStatusCode httpStatusCode, bool success, object data)
        {
            HttpStatusCode = httpStatusCode;
            Success = success;
            Data = data;
        }

        public ResponseBaseDTO(HttpStatusCode httpStatusCode, bool success, IEnumerable<string> errors)
        {
            HttpStatusCode = httpStatusCode;
            Success = success;
            Errors = errors;
        }

        public ResponseBaseDTO(HttpStatusCode httpStatusCode, bool success, object data, IEnumerable<string> errors)
        {
            HttpStatusCode = httpStatusCode;
            Success = success;
            Data = data;
            Errors = errors;
        }
    }
}