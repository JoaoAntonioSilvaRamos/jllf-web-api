﻿using JLLF.Domain.DTOs.Responses;
using JLLF.Presentation.ViewModels;
using System.Collections.Generic;
using System.Linq;

namespace JLLF.Presentation.Mappings.Responses
{
    public class ResponseCollaboratorViewModelMapping
    {
        private readonly SchedulingViewModelMapping schedulingViewModelMapping = new();

        public ResponseCollaboratorViewModel Conversion(ResponseCollaboratorDTO responseCollaboratorDTO)
        {
            return new ResponseCollaboratorViewModel
            {
                Id = responseCollaboratorDTO.Id,
                Name = responseCollaboratorDTO.Name,
                Gender = responseCollaboratorDTO.Gender,
                CPF = responseCollaboratorDTO.CPF,
                TelephoneNumber = responseCollaboratorDTO.TelephoneNumber,
                Function = responseCollaboratorDTO.Function,
                RegistrationDate = responseCollaboratorDTO.RegistrationDate,
                Status = responseCollaboratorDTO.Status,
                SchedulingsViewModelListByCollaboratorViewModel = schedulingViewModelMapping.CollectionConversion(responseCollaboratorDTO.SchedulingsDTOListByCollaboratorDTO)
            };
        }

        public IList<ResponseCollaboratorViewModel> ListConversion(IList<ResponseCollaboratorDTO> responseCollaboratorsDTO)
        {
            return responseCollaboratorsDTO.Select(item => Conversion(item)).ToList();
        }
    }
}