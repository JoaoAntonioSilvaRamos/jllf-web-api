﻿using JLLF.Domain.Interfaces.Services;
using JLLF.Domain.Validations;
using JLLF.Presentation.Mappings;
using JLLF.Presentation.Mappings.Responses;
using JLLF.Presentation.ViewModels;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;

namespace JLLF.Presentation.Controllers
{
    public class SchedulingsController : Controller
    {
        private readonly ISchedulingService _schedulingService;
        private readonly CompositeViewModelMapping compositeViewModelMapping = new();
        private readonly ResponseSchedulingViewModelMapping responseSchedulingViewModelMapping = new();

        public SchedulingsController(ISchedulingService schedulingService)
        {
            _schedulingService = schedulingService;
        }

        public IActionResult Index()
        {
            try
            {
                ViewBag.message = TempData["message"] as string;

                IList<ResponseSchedulingViewModel> responseSchedulingViewModels = responseSchedulingViewModelMapping.ListConversion(_schedulingService.FindAll());

                if (responseSchedulingViewModels.Count > 0)
                    return View(responseSchedulingViewModels);

                return RedirectToAction("Create");
            }
            catch (Exception)
            {
                return RedirectToAction("Create");
            }
        }

        public IActionResult Create()
        {
            try
            {
                CompositeViewModel compositeViewModel = compositeViewModelMapping.Conversion(_schedulingService.FindAllCollaboratorsAndCustomersAndServicesProvided());

                if (compositeViewModel != null)
                    return View(compositeViewModel);

                return RedirectToAction("Index");
            }
            catch (Exception)
            {
                return RedirectToAction("Index");
            }
        }

        [HttpPost]
        public IActionResult Create(CompositeViewModel compositeViewModel)
        {
            if (ModelState.IsValid)
            {
                compositeViewModel.Status = false;

                _schedulingService.Create<SchedulingValidation>(compositeViewModelMapping.Conversion(compositeViewModel));

                TempData["message"] = "Agendamento marcado com sucesso!".ToString();

                return RedirectToAction("Index");
            }

            return View();
        }

        public IActionResult Update(int id)
        {
            try
            {
                CompositeViewModel compositeViewModel = compositeViewModelMapping.ConversionAll(_schedulingService.FindCollaboratorAndCustomerAndSchedulingAndServiceProvidedById(id));

                if (compositeViewModel != null)
                    return View(compositeViewModel);

                return RedirectToAction("Index");
            }
            catch (Exception)
            {
                return RedirectToAction("Index");
            }
        }

        [HttpPost]
        public IActionResult Update(CompositeViewModel compositeViewModel)
        {
            if (ModelState.IsValid)
            {
                compositeViewModel.Status = false;

                _schedulingService.Update<SchedulingValidation>(compositeViewModelMapping.Conversion(compositeViewModel));

                TempData["message"] = "Agendamento remarcado com sucesso!".ToString();

                return RedirectToAction("Index");
            }

            return View();
        }

        public IActionResult UpdateStatus(int id)
        {
            try
            {
                CompositeViewModel compositeViewModel = compositeViewModelMapping.ConversionAll(_schedulingService.FindCollaboratorAndCustomerAndSchedulingAndServiceProvidedById(id));

                if (compositeViewModel != null)
                    return View(compositeViewModel);

                return RedirectToAction("Index");
            }
            catch (Exception)
            {
                return RedirectToAction("Index");
            }
        }

        [HttpPost]
        public IActionResult UpdateStatus(CompositeViewModel compositeViewModel)
        {
            if (ModelState.IsValid)
            {
                compositeViewModel.Status = true;

                _schedulingService.Update<SchedulingValidation>(compositeViewModelMapping.Conversion(compositeViewModel));

                return RedirectToAction("Index");
            }

            return View();
        }
    }
}