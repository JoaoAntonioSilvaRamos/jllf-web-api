﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace JLLF.Presentation.ViewModels
{
    public class CustomerViewModel
    {
        public int? Id { get; set; }

        [Required(ErrorMessage = "Campo Obrigatório!")]
        [MinLength(1, ErrorMessage = "Campo Deve Conter No Mínimo 1 Caracter!")]
        [MaxLength(255, ErrorMessage = "Campo Deve Conter No Máximo 255 Caracteres!")]
        public string Name { get; set; }

        [Required(ErrorMessage = "Campo Obrigatório!")]
        [MinLength(8, ErrorMessage = "Campo Deve Conter No Mínimo 8 Caracteres - Feminino!")]
        [MaxLength(9, ErrorMessage = "Campo Deve Conter No Máximo 9 Caracteres - Masculino!")]
        public string Gender { get; set; }

        [Required(ErrorMessage = "Campo Obrigatório!")]
        [StringLength(14, ErrorMessage = "Campo Deve Conter 14 Caracteres!", MinimumLength = 14)]
        [DisplayFormat(DataFormatString = "000.000.000-00")]
        public string CPF { get; set; }

        [Required(ErrorMessage = "Campo Obrigatório!")]
        [StringLength(13, ErrorMessage = "Campo Deve Conter 13 Caracteres!", MinimumLength = 13)]
        [DisplayFormat(DataFormatString = "00 00000-0000")]
        public string TelephoneNumber { get; set; }

        [Required(ErrorMessage = "Campo Obrigatório!")]
        [MinLength(1, ErrorMessage = "Campo Deve Conter No Mínimo 1 Caracter!")]
        [MaxLength(255, ErrorMessage = "Campo Deve Conter No Máximo 255 Caracteres!")]
        [DataType(DataType.EmailAddress)]
        [EmailAddress(ErrorMessage = "E-mail Em Formato Inválido!")]
        public string EmailAddress { get; set; }

        public DateTime? RegistrationDate { get; set; }

        public bool? Status { get; set; }
    }

    public class ResponseCustomerViewModel : CustomerViewModel
    {
        public virtual ICollection<SchedulingViewModel> SchedulingsViewModelListByCustomerViewModel { get; set; }
    }
}