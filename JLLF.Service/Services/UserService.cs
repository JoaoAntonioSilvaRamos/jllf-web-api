﻿using FluentValidation;
using JLLF.Domain.DTOs;
using JLLF.Domain.DTOs.Responses;
using JLLF.Domain.Entities;
using JLLF.Domain.Interfaces.Repositories;
using JLLF.Domain.Interfaces.Services;
using JLLF.Domain.Interfaces.UoW;
using JLLF.Infra.CrossCutting.Constants;
using JLLF.Infra.CrossCutting.Messages;
using JLLF.Service.Mappings;
using JLLF.Service.Mappings.Responses;
using System;
using System.Collections.Generic;

namespace JLLF.Service.Services
{
    public class UserService : IUserService
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IUserRepository _userRepository;
        private readonly UserMapping _userMapping = new();
        private readonly ResponseUserMapping _responseUserMapping = new();

        public UserService(IUnitOfWork unitOfWork, IUserRepository userRepository)
        {
            _unitOfWork = unitOfWork;
            _userRepository = userRepository;
        }

        public void Create<UserValidation>(UserDTO userDTO) where UserValidation : AbstractValidator<User>
        {
            var user = _userMapping.Conversion(userDTO);

            if (user.Id >= GeneralConstants.ONE)
                throw new Exception(GeneralMessages.IdMustBe_0.GetMessage("User"));

            Validate(user, Activator.CreateInstance<UserValidation>());

            _userRepository.Insert(user);

            _unitOfWork.Commit();
        }

        public void Update<UserValidation>(UserDTO userDTO) where UserValidation : AbstractValidator<User>
        {
            var user = _userMapping.Conversion(userDTO);

            if (user.Id <= GeneralConstants.ZERO)
                throw new Exception(GeneralMessages.IdMustBeGreatherThan_0.GetMessage("User"));

            Validate(user, Activator.CreateInstance<UserValidation>());

            _userRepository.Update(user);

            _unitOfWork.Commit();
        }

        public void Delete(int id)
        {
            if (id <= GeneralConstants.ZERO)
                throw new Exception(GeneralMessages.IdMustBeGreatherThan_0.GetMessage("User"));

            _userRepository.Delete(id);

            _unitOfWork.Commit();
        }

        public ResponseUserDTO FindById(int id)
        {
            if (id <= GeneralConstants.ZERO)
                throw new Exception(GeneralMessages.IdMustBeGreatherThan_0.GetMessage("User"));

            var userDB = _userRepository.FindById(id);

            var responseUserDTO = _responseUserMapping.Conversion(userDB);

            return responseUserDTO ?? throw new Exception(GeneralMessages.NotFound.GetMessage("User"));
        }

        public IList<ResponseUserDTO> FindAll()
        {
            var usersDB = _userRepository.FindAll();

            var responseUsersDTO = _responseUserMapping.ListConversion(usersDB);

            return responseUsersDTO != null && responseUsersDTO.Count > 0 ? responseUsersDTO : throw new Exception(GeneralMessages.NotFound.GetMessage("Users"));
        }

        public ResponseUserDTO FindByEmailAddressAndPassword(string emailAddress, string password)
        {
            var userDB = _userRepository.FindByEmailAddressAndPassword(emailAddress, password);

            if (userDB != null)
                return _responseUserMapping.Conversion(userDB);

            return null;
        }

        private static void Validate(User user, AbstractValidator<User> validator)
        {
            if (user == null)
                throw new Exception(GeneralMessages.NotFound.GetMessage("User"));

            validator.ValidateAndThrow(user);
        }
    }
}