﻿using JLLF.Domain.DTOs.Responses;
using JLLF.Domain.Entities;
using System.Collections.Generic;
using System.Linq;

namespace JLLF.Service.Mappings.Responses
{
    public class ResponseServiceProvidedMapping
    {
        private SchedulingMapping schedulingMapping = new();

        public ResponseServiceProvidedDTO Conversion(ServiceProvided serviceProvided)
        {
            return new ResponseServiceProvidedDTO
            {
                Id = serviceProvided.Id,
                Description = serviceProvided.Description,
                Price = serviceProvided.Price,
                RegistrationDate = serviceProvided.Registration.RegistrationDate,
                Status = serviceProvided.Condition.Status,
                SchedulingsDTOListByServiceProvidedDTO = schedulingMapping.CollectionConversion(serviceProvided.SchedulingsListByServiceProvided)
            };
        }

        public IList<ResponseServiceProvidedDTO> ListConversion(IList<ServiceProvided> servicesProvided)
        {
            return servicesProvided.Select(item => Conversion(item)).ToList();
        }
    }
}