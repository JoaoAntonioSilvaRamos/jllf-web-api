﻿using FluentValidation;
using JLLF.Domain.DTOs;
using JLLF.Domain.DTOs.Responses;
using JLLF.Domain.Entities;
using System.Collections.Generic;

namespace JLLF.Domain.Interfaces.Services
{
    public interface ICustomerService
    {
        void Create<CustomerValidation>(CustomerDTO customerDTO) where CustomerValidation : AbstractValidator<Customer>;
        void Update<CustomerValidation>(CustomerDTO customerDTO) where CustomerValidation : AbstractValidator<Customer>;
        void Delete(int id);
        ResponseCustomerDTO FindById(int id);
        IList<ResponseCustomerDTO> FindAll();
    }
}