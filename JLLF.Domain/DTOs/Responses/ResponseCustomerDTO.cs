﻿using System.Collections.Generic;

namespace JLLF.Domain.DTOs.Responses
{
    public class ResponseCustomerDTO : CustomerDTO
    {
        public virtual ICollection<SchedulingDTO> SchedulingsDTOListByCustomerDTO { get; set; }
    }
}