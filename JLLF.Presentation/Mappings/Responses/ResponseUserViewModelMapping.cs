﻿using JLLF.Domain.DTOs.Responses;
using JLLF.Presentation.ViewModels;

namespace JLLF.Presentation.Mappings.Responses
{
    public class ResponseUserViewModelMapping
    {
        public ResponseUserViewModel Conversion(ResponseUserDTO responseUserDTO)
        {
            return new ResponseUserViewModel
            {
                Id = responseUserDTO.Id,
                EmailAddress = responseUserDTO.EmailAddress,
                RegistrationDate = responseUserDTO.RegistrationDate,
                Status = responseUserDTO.Status
            };
        }
    }
}