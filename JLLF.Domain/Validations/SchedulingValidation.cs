﻿using FluentValidation;
using JLLF.Domain.Entities;
using System;

namespace JLLF.Domain.Validations
{
    public class SchedulingValidation : AbstractValidator<Scheduling>
    {
        public SchedulingValidation()
        {
            RuleFor(s => s)
                .NotNull()
                .OnAnyFailure(s =>
                {
                    throw new ArgumentNullException("Invalid Scheduling!");
                });

            RuleFor(s => s.IdCollaborator)
                .GreaterThan(0).WithMessage("Collaborator ID must be greater than 0!");

            RuleFor(s => s.IdCustomer)
                .GreaterThan(0).WithMessage("Customer ID must be greater than 0!");

            RuleFor(s => s.IdServiceProvided)
                .GreaterThan(0).WithMessage("Service Provided ID must be greater than 0!");

            RuleFor(s => s.DateTime)
                .NotEmpty().WithMessage("Schedule date and time cannot be empty!")
                .NotNull().WithMessage("Schedule date and time cannot be null!");
        }
    }
}