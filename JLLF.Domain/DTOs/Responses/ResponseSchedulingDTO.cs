﻿namespace JLLF.Domain.DTOs.Responses
{
    public class ResponseSchedulingDTO : SchedulingDTO
    {
        public virtual CollaboratorDTO CollaboratorDTO { get; set; }
        public virtual CustomerDTO CustomerDTO { get; set; }
        public virtual ServiceProvidedDTO ServiceProvidedDTO { get; set; }
    }
}