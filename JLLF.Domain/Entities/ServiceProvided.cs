﻿using JLLF.Domain.Entities.Base;
using JLLF.Domain.VOs;
using System;
using System.Collections.Generic;

namespace JLLF.Domain.Entities
{
    public class ServiceProvided : BaseEntity
    {
        public string Description { get; private set; }
        public decimal Price { get; private set; }
        public Registration Registration { get; private set; }
        public Condition Condition { get; private set; }
        public virtual ICollection<Scheduling> SchedulingsListByServiceProvided { get; private set; }

        public ServiceProvided(int? id, string description, decimal price, DateTime? registrationDate, bool? status)
        {
            Id = id;
            ValidateEmptyDescription(description);
            ValidateNullDescription(description);
            ValidateDescriptionLength(description);
            Description = description;
            ValidateEmptyPrice(price);
            ValidateNullPrice(price);
            Price = price;
            AssignRegistration(registrationDate);
            AssignCondition(status);
        }

        protected ServiceProvided()
        {

        }

        private static void ValidateEmptyDescription(string description)
        {
            if (description == string.Empty)
                throw new ArgumentException("Description cannot be empty!");
        }

        private static void ValidateNullDescription(string description)
        {
            if (description == null)
                throw new ArgumentException("Description cannot be null!");
        }

        private static void ValidateDescriptionLength(string description)
        {
            if (description.Length > 255)
                throw new ArgumentException("Description cannot be longer than 255 characters!");
        }

        private static void ValidateEmptyPrice(decimal price)
        {
            if (price.ToString() == string.Empty)
                throw new ArgumentException("Price cannot be empty!");
        }

        private static void ValidateNullPrice(decimal price)
        {
            if (price.ToString() == null)
                throw new ArgumentException("Price cannot be null!");
        }

        private bool AssignRegistration(DateTime? registrationDate)
        {
            var registration = new Registration(registrationDate);

            Registration = registration;

            return true;
        }

        private bool AssignCondition(bool? status)
        {
            var condition = new Condition(status);

            Condition = condition;

            return true;
        }
    }
}