﻿using JLLF.Domain.DTOs.Responses;
using JLLF.Presentation.ViewModels;
using System.Collections.Generic;
using System.Linq;

namespace JLLF.Presentation.Mappings.Responses
{
    public class ResponseCustomerViewModelMapping
    {
        private SchedulingViewModelMapping schedulingViewModelMapping = new();

        public ResponseCustomerViewModel Conversion(ResponseCustomerDTO responseCustomerDTO)
        {
            return new ResponseCustomerViewModel
            {
                Id = responseCustomerDTO.Id,
                Name = responseCustomerDTO.Name,
                Gender = responseCustomerDTO.Gender,
                CPF = responseCustomerDTO.CPF,
                TelephoneNumber = responseCustomerDTO.TelephoneNumber,
                EmailAddress = responseCustomerDTO.EmailAddress,
                RegistrationDate = responseCustomerDTO.RegistrationDate,
                Status = responseCustomerDTO.Status,
                SchedulingsViewModelListByCustomerViewModel = schedulingViewModelMapping.CollectionConversion(responseCustomerDTO.SchedulingsDTOListByCustomerDTO)
            };
        }

        public IList<ResponseCustomerViewModel> ListConversion(IList<ResponseCustomerDTO> responseCustomersDTO)
        {
            return responseCustomersDTO.Select(item => Conversion(item)).ToList();
        }
    }
}