﻿using JLLF.Domain.Interfaces.Services;
using JLLF.Domain.Validations;
using JLLF.Presentation.Mappings;
using JLLF.Presentation.Mappings.Responses;
using JLLF.Presentation.ViewModels;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;

namespace JLLF.Presentation.Controllers
{
    public class CollaboratorsController : Controller
    {
        private readonly ICollaboratorService _collaboratorService;
        private readonly CollaboratorViewModelMapping collaboratorViewModelMapping = new();
        private readonly ResponseCollaboratorViewModelMapping responseCollaboratorViewModelMapping = new();

        public CollaboratorsController(ICollaboratorService collaboratorService)
        {
            _collaboratorService = collaboratorService;
        }

        public IActionResult Index()
        {
            try
            {
                ViewBag.message = TempData["message"] as string;

                IList<ResponseCollaboratorViewModel> responseCollaboratorViewModels = responseCollaboratorViewModelMapping.ListConversion(_collaboratorService.FindAll());

                if (responseCollaboratorViewModels.Count > 0)
                    return View(responseCollaboratorViewModels);

                return RedirectToAction("Create");
            }
            catch (Exception)
            {
                return RedirectToAction("Create");
            }
        }

        public IActionResult Create()
        {
            return View();
        }

        [HttpPost]
        public IActionResult Create(CollaboratorViewModel collaboratorViewModel)
        {
            if (ModelState.IsValid)
            {
                collaboratorViewModel.RegistrationDate = DateTime.Now;
                collaboratorViewModel.Status = true;

                _collaboratorService.Create<CollaboratorValidation>(collaboratorViewModelMapping.Conversion(collaboratorViewModel));

                TempData["message"] = "Colaborador cadastrado com sucesso!".ToString();

                return RedirectToAction("Index");
            }

            return View();
        }

        public IActionResult Update(int id)
        {
            try
            {
                ResponseCollaboratorViewModel responseCollaboratorViewModel = responseCollaboratorViewModelMapping.Conversion(_collaboratorService.FindById(id));

                if (responseCollaboratorViewModel.Id > 0)
                    return View(responseCollaboratorViewModel);

                return RedirectToAction("Index");
            }
            catch (Exception)
            {
                return RedirectToAction("Index");
            }
        }

        [HttpPost]
        public IActionResult Update(CollaboratorViewModel collaboratorViewModel)
        {
            if (ModelState.IsValid)
            {
                collaboratorViewModel.RegistrationDate = DateTime.Now;
                collaboratorViewModel.Status = true;

                _collaboratorService.Update<CollaboratorValidation>(collaboratorViewModelMapping.Conversion(collaboratorViewModel));

                TempData["message"] = "Colaborador atualizado com sucesso!".ToString();

                return RedirectToAction("Index");
            }

            return View();
        }

        public IActionResult UpdateStatus(int id)
        {
            try
            {
                ResponseCollaboratorViewModel responseCollaboratorViewModel = responseCollaboratorViewModelMapping.Conversion(_collaboratorService.FindById(id));

                if (responseCollaboratorViewModel.Id > 0)
                    return View(responseCollaboratorViewModel);

                return RedirectToAction("Index");
            }
            catch (Exception)
            {
                return RedirectToAction("Index");
            }
        }

        [HttpPost]
        public IActionResult UpdateStatus(CollaboratorViewModel collaboratorViewModel)
        {
            if (ModelState.IsValid)
            {
                collaboratorViewModel.RegistrationDate = DateTime.Now;
                collaboratorViewModel.Status = false;

                _collaboratorService.Update<CollaboratorValidation>(collaboratorViewModelMapping.Conversion(collaboratorViewModel));

                return RedirectToAction("Index");
            }

            return View();
        }

        public IActionResult Details(int id)
        {
            try
            {
                ResponseCollaboratorViewModel responseCollaboratorViewModel = responseCollaboratorViewModelMapping.Conversion(_collaboratorService.FindById(id));

                if (responseCollaboratorViewModel.Id > 0)
                    return View(responseCollaboratorViewModel);

                return RedirectToAction("Index");
            }
            catch (Exception)
            {
                return RedirectToAction("Index");
            }
        }
    }
}