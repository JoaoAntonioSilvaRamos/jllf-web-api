﻿using System;

namespace JLLF.Domain.DTOs
{
    public class CustomerDTO
    {
        public int? Id { get; set; }
        public string Name { get; set; }
        public string Gender { get; set; }
        public string CPF { get; set; }
        public string TelephoneNumber { get; set; }
        public string EmailAddress { get; set; }
        public DateTime? RegistrationDate { get; set; }
        public bool? Status { get; set; }
    }
}